<?php

namespace GbsLogistics\Crest\Test\Functional;


use GbsLogistics\Crest\Client\Client;
use GbsLogistics\Crest\CrestClient;
use GbsLogistics\Crest\OptionsBuilder;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

class BaseFunctionalTestCase extends \PHPUnit_Framework_TestCase
{
    /** @var array */
    private $container = [];

    protected function loadFixtureAndGetClient(string $jsonFilepath, string $expectedResourceName, int $expectedVersion): Client
    {
        $contentType = sprintf('application/vnd.ccp.eve.%s-v%s+json', $expectedResourceName, $expectedVersion);
        $fixtureData = file_get_contents($jsonFilepath);
        $stack = HandlerStack::create(new MockHandler([
            new Response(200, [ 'Content-Type' => $contentType ]),
            new Response(200, [ 'Content-Type' => $contentType ], $fixtureData)
        ]));

        $stack->push(Middleware::history($this->container));

        $optionsBuilder = new OptionsBuilder('https://public-crest.eveonline.com/');
        $optionsBuilder->setHandlerStack($stack);

        return (new CrestClient())->instance($optionsBuilder->build());
    }

    protected function assertRequests(array $requests)
    {
        foreach ($requests as $index => $expectedMethod) {
            $transaction = $this->container[$index] ?? null;

            if (null === $transaction) {
                $this->fail(sprintf(
                    'Expected to receive %s response%s to request, but %s.',
                    count($requests),
                    1 === count($requests) ? '' : 's',
                    0 === $index ? 'didn\'t receive any' : sprintf('only received %s', $index)
                ));
                break;
            }

            /** @var Request $request */
            $request = $transaction['request'];
            $actualMethod = $request->getMethod();

            if ($actualMethod !== $expectedMethod) {
                $this->fail(sprintf('Expected request #%s to be "%s", got "%s" instead.', $index + 1, $expectedMethod, $actualMethod));
            }
        }
    }

    protected function assertObjectsHaveData(array $expectedObjects, array $actualObjects, callable $mapLambda, float $floatPrecision = 0.0001)
    {
        $actual = array_map($mapLambda, $actualObjects);

        foreach ($expectedObjects as $index => $expectedItem) {
            $actualItem = $actual[$index];

            if (is_float($expectedItem)) {
                $this->assertEquals($expectedItem, $actualItem, $floatPrecision);
            } else {
                $this->assertEquals($expectedItem, $actualItem);
            }
        }
    }
}