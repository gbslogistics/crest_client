<?php

namespace GbsLogistics\Crest\Test\Functional;


use GbsLogistics\Crest\Domain\IndustrySystemCollectionMember;
use GbsLogistics\Crest\Domain\Partial\PartialSolarSystem;
use GbsLogistics\Crest\Domain\Partial\SystemCostIndex;

class IndustrySystemCollectionTest extends BaseFunctionalTestCase
{
    /** @group functional */
    public function testIndustrySystemCollectionEndpoint()
    {
        $client = $this->loadFixtureAndGetClient(__DIR__ . '/../fixtures/industry_system_collection.json', 'IndustrySystemCollection', 1);

        /** @var IndustrySystemCollectionMember $response */
        $response = $client->getByHref('https://public-crest.eveonline.com/industry/systems/');
        $this->assertRequests(['HEAD', 'GET']);
        $this->assertInstanceOf(IndustrySystemCollectionMember::class, $response);

        $systemCostIndices = $response->getSystemCostIndices();
        $this->assertInternalType('array', $systemCostIndices);
        $this->assertContainsOnlyInstancesOf(SystemCostIndex::class, $systemCostIndices);

        $this->assertObjectsHaveData(
            ['Invention', 'Manufacturing', 'Researching Time Efficiency', 'Researching Material Efficiency', 'Copying'],
            $systemCostIndices,
            function (SystemCostIndex $index) { return $index->getActivityName(); }
        );

        $this->assertObjectsHaveData(
            [0.00098, 0.025244680049825982, 0.002890598321132683, 0.008320078417628012, 0.00098],
            $systemCostIndices,
            function (SystemCostIndex $index) { return $index->getCostIndex(); }
        );

        $this->assertObjectsHaveData(
            [8, 1, 3, 4, 5],
            $systemCostIndices,
            function (SystemCostIndex $index) { return $index->getActivityID(); }
        );

        $partialSolarSystem = $response->getPartialSolarSystem();
        $this->assertInstanceOf(PartialSolarSystem::class, $partialSolarSystem);

        $this->assertEquals('30011392', $partialSolarSystem->getId());
        $this->assertEquals('Jouvulen', $partialSolarSystem->getName());
    }
}