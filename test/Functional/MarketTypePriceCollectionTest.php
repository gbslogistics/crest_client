<?php

namespace GbsLogistics\Crest\Test\Functional;


use GbsLogistics\Crest\Domain\MarketTypePriceCollectionMember;

class MarketTypePriceCollectionTest extends BaseFunctionalTestCase
{
    /** @group functional */
    public function testMarketTypePriceCollectionEndpoint()
    {
        $client = $this->loadFixtureAndGetClient(__DIR__ . '/../fixtures/market_type_price_collection.json', 'MarketTypePriceCollection', 1);
        /** @var MarketTypePriceCollectionMember $response */
        $response = $client->getByHref('https://public-crest.eveonline.com/market/prices/');
        $this->assertRequests(['HEAD', 'GET']);

        $this->assertInstanceOf(MarketTypePriceCollectionMember::class, $response);
        $this->assertEquals(304841.79, $response->getAdjustedPrice(), 0.01);
        $this->assertEquals(304321.23, $response->getAveragePrice(), 0.01);
        $this->assertEquals('32772', $response->getId());
        $this->assertEquals('Medium Ancillary Shield Booster', $response->getName());
    }
}