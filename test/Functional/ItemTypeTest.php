<?php

namespace GbsLogistics\Crest\Test\Functional;


use GbsLogistics\Crest\Domain\ItemType;
use GbsLogistics\Crest\Domain\Partial\PartialDogmaAttribute;

class ItemTypeTest extends BaseFunctionalTestCase
{
    /** @group functional */
    public function testItemTypeEndpoint()
    {
        $client = $this->loadFixtureAndGetClient(
            __DIR__ . '/../fixtures/item_type.json',
            'ItemType',
            3
        );

        /** @var ItemType $response */
        $response = $client->getByHref('https://public-crest.eveonline.com/types/34207/');
        $this->assertInstanceOf(ItemType::class, $response);
        $this->assertEquals(0.0, $response->getCapacity(), 0.1);
        $this->assertEquals(
            'A rare and valuable decryptor that dramatically increases your chance for success at invention. Gives minor benefit to mineral efficiency at the expense of slight increase in production time.<br><br>Probability Multiplier: +90% <br>Max. Run Modifier: +2 <br>Material Efficiency Modifier: +1 <br>Time Efficiency Modifier: -2',
            $response->getDescription()
        );

        $this->assertEquals('1', $response->getPortionSize());
        $this->assertEquals(0.1, $response->getVolume(), 0.1);
        $this->assertEquals(1.0, $response->getRadius(), 0.1);
        $this->assertTrue($response->isPublished());
        $this->assertEquals(2885, $response->getIconID());
        $this->assertEquals(1.0, $response->getMass(), 0.1);
        $this->assertEquals('Optimized Attainment Decryptor', $response->getName());
        $this->assertEquals('34207', $response->getId());

        $this->assertObjectsHaveData(
            [ 1112, 1113, 1114, 1124 ],
            $response->getDogmaAttributes(),
            function (PartialDogmaAttribute $attribute) {
                return $attribute->getId();
            }
        );

        $this->assertObjectsHaveData(
            ['inventionPropabilityMultiplier', 'inventionMEModifier', 'inventionTEModifier', 'inventionMaxRunModifier'],
            $response->getDogmaAttributes(),
            function (PartialDogmaAttribute $attribute) {
                return $attribute->getName();
            }
        );

        $this->assertObjectsHaveData(
            [ 1.9, 1.0, -2.0, 2.0 ],
            $response->getDogmaAttributes(),
            function (PartialDogmaAttribute $attribute) {
                return $attribute->getValue();
            },
            0.1
        );
    }
}