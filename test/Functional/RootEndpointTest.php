<?php


namespace GbsLogistics\Crest\Test\Functional;


use GbsLogistics\Crest\Domain\Root;

/**
 * Class RootEndpointTest
 * @package GbsLogistics\Crest\Test\Functional
 */
class RootEndpointTest extends BaseFunctionalTestCase
{
    /**
     * @group functional
     */
    public function testGetRootEndpoint()
    {
        $client = $this->loadFixtureAndGetClient(__DIR__ . '/../fixtures/crest_root.json', 'Api', 3);

        /** @var Root $response */
        $response = $client->getByHref('/');
        $this->assertRequests(['HEAD', 'GET']);

        $this->assertInstanceOf(Root::class, $response);
        $this->assertEquals('1579', $response->getDustUserCount());
        $this->assertEquals('28689', $response->getEveUserCount());
        $this->assertEquals('EVE-TRANQUILITY 14.01.998647.998644', $response->getServerVersion());
        $this->assertEquals('online', $response->getDustStatus());
        $this->assertEquals('online', $response->getEveStatus());
        $this->assertEquals('online', $response->getServerStatus());
        $this->assertEquals('TRANQUILITY', $response->getServerName());

    }
}
