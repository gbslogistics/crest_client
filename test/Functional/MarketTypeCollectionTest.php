<?php

namespace GbsLogistics\Crest\Test\Functional;


use GbsLogistics\Crest\Domain\MarketTypeCollectionMember;

class MarketTypeCollectionTest extends BaseFunctionalTestCase
{
    /** @group functional */
    public function testMarketTypeCollectionEndpoint()
    {
        $client = $this->loadFixtureAndGetClient(
            __DIR__ . '/../fixtures/market_type_collection.json',
            'MarketTypeCollection',
            1
        );

        $response = $client->getByHref('https://public-crest.eveonline.com/market/types/?group=https://public-crest.eveonline.com/market/groups/1873/');
        $this->assertInternalType('array', $response);
        $this->assertContainsOnlyInstancesOf(MarketTypeCollectionMember::class, $response);
        $this->assertCount(8, $response);

        $this->assertObjectsHaveData(
            [ 34201, 34202, 34203, 34204, 34205, 34206, 34207, 34208 ],
            $response,
            function (MarketTypeCollectionMember $member) {
                return $member->getTypeId();
            }
        );
    }
}