<?php

namespace GbsLogistics\Crest\Test\Functional;


use GbsLogistics\Crest\Domain\MarketGroupCollectionMember;

class MarketGroupCollectionTest extends BaseFunctionalTestCase
{
    /** @group functional */
    public function testMarketGroupCollectionEndpoint()
    {
        $client = $this->loadFixtureAndGetClient(
            __DIR__ . '/../fixtures/market_group_collection.json',
            'MarketGroupCollection',
            1
        );

        /** @var MarketGroupCollectionMember[] $response */
        $response = $client->getByHref('https://public-crest.eveonline.com/market/groups/');
        $this->assertRequests(['HEAD', 'GET']);
        $this->assertInternalType('array', $response);
        $this->assertCount(1970, $response);
        $this->assertContainsOnlyInstancesOf(MarketGroupCollectionMember::class, $response);

        $member = $response[0];
        $this->assertEquals('Blueprints', $member->getName());
        $this->assertEquals('2', $member->getId());
        $this->assertEquals('Blueprints are data items used in industry for manufacturing, research and invention jobs', $member->getDescription());
    }
}