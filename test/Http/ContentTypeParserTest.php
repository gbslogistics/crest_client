<?php


namespace GbsLogistics\Crest\Test\Http;


use GbsLogistics\Crest\Http\ContentTypeParser;
use GbsLogistics\Crest\Http\Model\ContentType;
use Phake;

class ContentTypeParserTest extends \PHPUnit_Framework_TestCase
{
    /** @var ContentTypeParser */
    private $sut;

    /**
     * @var \Psr\Http\Message\ResponseInterface
     * @Mock
     */
    private $response;

    public function setUp()
    {
        Phake::initAnnotations($this);

        $this->sut = new ContentTypeParser();
    }

    /** @dataProvider successfulParseProvider */
    public function testSuccessfulParse($header, $expectedResourceName, $expectedVersion, $expectedIsCollection)
    {
        Phake::when($this->response)->getHeader(ContentTypeParser::CONTENT_TYPE_HEADER)->thenReturn([$header]);

        $contentType = $this->sut->parse($this->response);

        $this->assertInstanceOf(ContentType::class, $contentType);
        $this->assertEquals($expectedResourceName, $contentType->getResourceName());
        $this->assertEquals($expectedVersion, $contentType->getVersion());
        $this->assertEquals($expectedIsCollection, $contentType->isCollection());
    }

    public function testBadParse()
    {
        Phake::when($this->response)->getHeader(ContentTypeParser::CONTENT_TYPE_HEADER)->thenReturn(['text/plain']);

        $contentType = $this->sut->parse($this->response);
        $this->assertNull($contentType);
    }

    public function successfulParseProvider()
    {
        return [
            [ 'application/vnd.ccp.eve.Planet-v1+json; charset=utf-8', 'Planet', '1', false ],
            [ 'application/vnd.ccp.eve.PlanetCollection-v42+json; charset=utf-8', 'PlanetCollection', '42', true ],
        ];
    }
}
