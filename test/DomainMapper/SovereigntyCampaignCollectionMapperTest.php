<?php


namespace GbsLogistics\Crest\Test\DomainMapper;


use GbsLogistics\Crest\Domain\Partial\PartialAlliance;
use GbsLogistics\Crest\Domain\Partial\PartialSolarSystem;
use GbsLogistics\Crest\Domain\SovereigntyCampaign\Attackers;
use GbsLogistics\Crest\Domain\SovereigntyCampaign\Defender;
use GbsLogistics\Crest\Domain\SovereigntyCampaign\FreeportContestant;
use GbsLogistics\Crest\Domain\SovereigntyCampaignCollectionMember;
use GbsLogistics\Crest\DomainMapper\Mapper\SovereigntyCampaignCollectionMapper;
use GbsLogistics\Crest\Json\IJsonData;
use GbsLogistics\Crest\Json\InMemoryJsonData;
use GbsLogistics\Crest\Proxy\Proxy;

class SovereigntyCampaignCollectionMapperTest extends \PHPUnit_Framework_TestCase
{
    /** @var SovereigntyCampaignCollectionMapper */
    private $sut;

    public function setUp()
    {
        $this->sut = new SovereigntyCampaignCollectionMapper();
    }

    public function testMapStationContest()
    {
        /** @var SovereigntyCampaignCollectionMember $campaign */
        $campaign = $this->callSut('sov_campaign_station.json');

        $this->assertInstanceOf(SovereigntyCampaignCollectionMember::class, $campaign);
        $this->assertEquals(SovereigntyCampaignCollectionMember::STATION, $campaign->getEventType());
        $this->assertEquals('60014926', $campaign->getSourceItemID());
        $this->assertEquals('2', $campaign->getCampaignID());
        $this->assertEquals(new \DateTime('2015-06-29T09:56:13', new \DateTimeZone('UTC')), $campaign->getStartTime());

        $solarSystem = $campaign->getSourceSolarSystem();
        $this->assertInstanceOf(PartialSolarSystem::class, $solarSystem);
        $this->assertEquals('30004712', $solarSystem->getId());
        $this->assertEquals('NOL-M9', $solarSystem->getName());
        $this->assertContainsOnlyInstancesOf(Proxy::class, $solarSystem->getProxies());
        $this->assertCount(1, $solarSystem->getProxies());

        $defender = $campaign->getDefender();
        $this->assertInstanceOf(Defender::class, $defender);
        $this->assertEquals(0.5, $defender->getScore(), 0.01);
        $defenderAlliance = $defender->getAlliance();
        $this->assertInstanceOf(PartialAlliance::class, $defenderAlliance);
        $this->assertEquals('99000002', $defenderAlliance->getAllianceID());
        $this->assertEquals('One One Corporation Alliance', $defenderAlliance->getAllianceName());

        $attackers = $campaign->getAttackers();
        $this->assertInstanceOf(Attackers::class, $attackers);
        $this->assertEquals(0.5, $attackers->getScore(), 0.01);
    }

    public function testMapFreeportContest()
    {
        /** @var SovereigntyCampaignCollectionMember $campaign */
        $campaign = $this->callSut('sov_campaign_freeport.json');

        $this->assertInstanceOf(SovereigntyCampaignCollectionMember::class, $campaign);
        $this->assertEquals(SovereigntyCampaignCollectionMember::FREEPORT, $campaign->getEventType());
        $this->assertEquals('60014950', $campaign->getSourceItemID());
        $this->assertEquals('3', $campaign->getCampaignID());
        $this->assertEquals(new \DateTime('2015-06-29T15:09:59', new \DateTimeZone('UTC')), $campaign->getStartTime());

        $solarSystem = $campaign->getSourceSolarSystem();
        $this->assertInstanceOf(PartialSolarSystem::class, $solarSystem);
        $this->assertEquals('30004720', $solarSystem->getId());
        $this->assertEquals('0N-3RO', $solarSystem->getName());
        $this->assertContainsOnlyInstancesOf(Proxy::class, $solarSystem->getProxies());
        $this->assertCount(1, $solarSystem->getProxies());

        $this->assertNull($campaign->getDefender());

        $antagonists = $campaign->getFreeportContestants();
        $this->assertContainsOnlyInstancesOf(FreeportContestant::class, $antagonists);
        $this->assertCount(1, $antagonists);

        $contestant = $antagonists[0];
        $this->assertEquals(0.05, $contestant->getScore(), 0.01);
        $alliance = $contestant->getAlliance();
        $this->assertInstanceOf(PartialAlliance::class, $alliance);
        $this->assertEquals('99000002', $alliance->getAllianceID());
        $this->assertEquals('One One Corporation Alliance', $alliance->getAllianceName());
    }

    public function testMapInactiveFreeportContest()
    {
        /** @var SovereigntyCampaignCollectionMember $campaign */
        $campaign = $this->callSut('sov_campaign_inactive_freeport.json');

        $this->assertInstanceOf(SovereigntyCampaignCollectionMember::class, $campaign);
        $this->assertEquals(SovereigntyCampaignCollectionMember::FREEPORT, $campaign->getEventType());
        $this->assertEquals('61000432', $campaign->getSourceItemID());
        $this->assertEquals('20680', $campaign->getCampaignID());
        $this->assertEquals(new \DateTime('2016-04-27T02:49:44', new \DateTimeZone('UTC')), $campaign->getStartTime());

        $solarSystem = $campaign->getSourceSolarSystem();
        $this->assertInstanceOf(PartialSolarSystem::class, $solarSystem);
        $this->assertEquals('30000244', $solarSystem->getId());
        $this->assertEquals('K8X-6B', $solarSystem->getName());
        $this->assertContainsOnlyInstancesOf(Proxy::class, $solarSystem->getProxies());
        $this->assertCount(1, $solarSystem->getProxies());

        $this->assertNull($campaign->getDefender());

        $antagonists = $campaign->getFreeportContestants();
        $this->assertCount(0, $antagonists);
    }

    private function callSut($filename)
    {
        $jsonData = new InMemoryJsonData(json_decode(file_get_contents(__DIR__ . '/../fixtures/' . $filename), true));
        return $this->sut->map($jsonData);
    }
}
