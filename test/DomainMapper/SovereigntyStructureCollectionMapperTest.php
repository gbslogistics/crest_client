<?php


namespace GbsLogistics\Crest\Test\DomainMapper;


use GbsLogistics\Crest\Domain\Partial\PartialAlliance;
use GbsLogistics\Crest\Domain\Partial\PartialItemType;
use GbsLogistics\Crest\Domain\Partial\PartialSolarSystem;
use GbsLogistics\Crest\Domain\SovereigntyStructureCollectionMember;
use GbsLogistics\Crest\DomainMapper\Mapper\SovereigntyStructureCollectionMapper;
use GbsLogistics\Crest\Json\InMemoryJsonData;

class SovereigntyStructureCollectionMapperTest extends \PHPUnit_Framework_TestCase
{
    /** @var SovereigntyStructureCollectionMapper */
    private $sut;

    public function setUp()
    {
        $this->sut = new SovereigntyStructureCollectionMapper();
    }

    public function testMap()
    {
        $jsonData = new InMemoryJsonData(
            json_decode(file_get_contents(__DIR__ . '/../fixtures/sovereignty_structures.json'), true)
        );

        /** @var SovereigntyStructureCollectionMember $results */
        $results = $this->sut->map($jsonData);

        $this->assertInstanceOf(SovereigntyStructureCollectionMember::class, $results);
        $this->assertInstanceOf(PartialSolarSystem::class, $solarSystem = $results->getPartialSolarSystem());
        $this->assertInstanceOf(PartialItemType::class, $itemType = $results->getPartialItemType());
        $this->assertInstanceOf(PartialAlliance::class, $alliance = $results->getPartialAlliance());

        $this->assertEquals('1354830081', $alliance->getAllianceID());
        $this->assertEquals('Goonswarm Federation', $alliance->getAllianceName());

        $this->assertEquals('30002941', $solarSystem->getId());
        $this->assertEquals('43B-O1', $solarSystem->getName());

        $this->assertEquals('32226', $itemType->getTypeID());
        $this->assertEquals('Territorial Claim Unit', $itemType->getTypeName());

        $this->assertEquals(4.5, $results->getOccupancyLevel());
        $this->assertEquals('183219893', $results->getStructureID());
        $startTime = $results->getVulnerableStartTime();
        $this->assertEquals('2016-03-30T17:00:00', $startTime->format('Y-m-d\TH:i:s'));
        $endTime = $results->getVulnerableEndTime();
        $this->assertEquals('2016-03-30T21:00:00', $endTime->format('Y-m-d\TH:i:s'));
    }
}
