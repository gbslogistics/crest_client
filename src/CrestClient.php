<?php

namespace GbsLogistics\Crest;


use GbsLogistics\Crest\Client\ClientFactory;
use GbsLogistics\Crest\Client\IClient;
use GbsLogistics\Crest\Client\IClientFactory;
use GbsLogistics\Crest\Client\Model\IOptions;
use GbsLogistics\Crest\DependencyInjection\ContainerFactory;

class CrestClient
{
    /**
     * Provides an instance of the CREST client.
     *
     * @param IOptions $options
     * @return IClient
     */
    public static function instance(IOptions $options): IClient
    {
        $container = ContainerFactory::getContainer();

        /** @var IClientFactory $clientFactory */
        $clientFactory = $container[ClientFactory::class];

        return $clientFactory->getClient($options);
    }
}