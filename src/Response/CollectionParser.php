<?php

namespace GbsLogistics\Crest\Response;


use GbsLogistics\Crest\Json\IJsonData;
use GbsLogistics\Crest\Response\Model\IResponseFactory;

class CollectionParser
{
    /**
     * @param IJsonData $jsonData
     * @return \Generator Generator yields instances of IJsonData. If there are multiple pages, the generator will
     *     return the href for the next page via \Generator::getReturn(), or null otherwise.
     */
    public function parse(IJsonData $jsonData): \Generator
    {
        $items = $jsonData->getJsonDataByFirstLevelKey('items');

        if ($items && $items instanceof IJsonData) {
            yield from $items->getRootGenerator();
        }

        if (($next = $jsonData->getJsonDataByFirstLevelKey('next')) && $next instanceof IJsonData) {
            return $next->getJsonDataByFirstLevelKey('href');
        }

        return null;
    }
}