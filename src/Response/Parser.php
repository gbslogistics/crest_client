<?php

namespace GbsLogistics\Crest\Response;


use GbsLogistics\Crest\DomainMapper\MapperRegistry;
use GbsLogistics\Crest\Response\Model\PooledRequest;
use GbsLogistics\Crest\Response\Model\WrappedResponse;
use Psr\Http\Message\ResponseInterface;

class Parser
{
    /** @var ResponsePreprocessor */
    private $preprocessor;

    /** @var MapperRegistry */
    private $mapperRegistry;

    /** @var CollectionParser */
    private $collectionParser;

    /**
     * Parser constructor.
     * @param ResponsePreprocessor $preprocessor
     * @param MapperRegistry $mapperRegistry
     * @param CollectionParser $collectionParser
     */
    public function __construct(ResponsePreprocessor $preprocessor, MapperRegistry $mapperRegistry, CollectionParser $collectionParser)
    {
        $this->preprocessor = $preprocessor;
        $this->mapperRegistry = $mapperRegistry;
        $this->collectionParser = $collectionParser;
    }

    public function parse(ResponseInterface $rawResponse): WrappedResponse
    {
        $genericResponse = $this->preprocessor->parse($rawResponse);
        $contentType = $genericResponse->getContentType();

        if (null !== $contentType) {
            $mapper = $this->mapperRegistry->getMapperByResourceName($contentType->getResourceName());
            if (null !== $mapper) {
                if ($contentType->isCollection()) {
                    $responseObjects = [];
                    $generator = $this->collectionParser->parse($genericResponse->getJsonData());

                    foreach ($generator as $jsonData) {
                        $responseObjects[] = $mapper->map($jsonData);
                    }

                    $nextPage = $generator->getReturn();

                    return new WrappedResponse(
                        $responseObjects,
                        null === $nextPage ? [] : [ new PooledRequest($nextPage) ],
                        $genericResponse->getJsonData()
                    );
                } else {
                    return new WrappedResponse(
                        $mapper->map($genericResponse->getJsonData()),
                        [],
                        $genericResponse->getJsonData()
                    );
                }
            }
        }

        return new WrappedResponse($genericResponse);
    }
}