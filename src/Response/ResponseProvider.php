<?php

namespace GbsLogistics\Crest\Response;


use GbsLogistics\Crest\DomainMapper\MapperRegistry;
use GbsLogistics\Crest\Http\ContentTypeParser;
use GbsLogistics\Crest\Json\NativeJsonParser;
use GbsLogistics\Crest\Response\Factory\RootResponseFactory;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ResponseProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        $pimple[RootResponseFactory::class] = function ($c) {
            return new RootResponseFactory(
                $c[NativeJsonParser::class]
            );
        };

        $pimple[ResponsePreprocessor::class] = function ($c) {
            return new ResponsePreprocessor(
                $c['json_parser'],
                $c[ContentTypeParser::class]
            );
        };
        $pimple[Parser::class] = function ($c) {
            return new Parser(
                $c[ResponsePreprocessor::class],
                $c[MapperRegistry::class],
                $c[CollectionParser::class]
            );
        };

        $pimple[CollectionParser::class] = function ($c) {
            return new CollectionParser();
        };
    }
}