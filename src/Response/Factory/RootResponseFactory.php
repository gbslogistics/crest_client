<?php

namespace GbsLogistics\Crest\Response\Factory;


use GbsLogistics\Crest\DomainMapper\Mapper\RootMapper;
use GbsLogistics\Crest\Domain\Root;
use GbsLogistics\Crest\Json\IJsonData;
use GbsLogistics\Crest\Json\IJsonParser;
use GbsLogistics\Crest\Response\RootResponse;
use Psr\Http\Message\ResponseInterface;

class RootResponseFactory
{
    /** @var IJsonParser */
    private $jsonParser;

    /** @var array */
    private $resources = [];

    /**
     * RootResponseFactory constructor.
     * @param IJsonParser $jsonParser
     */
    public function __construct(IJsonParser $jsonParser)
    {
        $this->jsonParser = $jsonParser;
    }

    /**
     * Supplies which CREST resource this factory can handle.
     *
     * @return string
     */
    public function getContentType(): string
    {
        return 'application/vnd.ccp.eve.Api-v3+json';
    }

    public function build(IJsonData $jsonData, Root $data): RootResponse
    {
        $this->traverse('', $jsonData->getRootGenerator());

        return new RootResponse($this->resources, $data);
    }

    /**
     * @param string $pathPrefix
     * @param array|\Generator $jsonData
     */
    private function traverse(string $pathPrefix, $jsonData)
    {
        /**
         * @var string $key
         * @var mixed $data
         */
        foreach ($jsonData as $key => $data) {
            $path = strlen($pathPrefix) > 0 ? sprintf('%s.%s', $pathPrefix, $key) : $key;

            if ('href' === $key) {
                $this->resources[$pathPrefix] = $data;
            } elseif (is_array($data)) {
                $this->traverse($path, $data);
            } elseif (is_object($data)) {
                if ($data instanceof \Generator) {
                    $this->traverse($path, $data);
                } elseif ($data instanceof IJsonData) {
                    /** @var IJsonData $data */
                    $this->traverse($path, $data->getRootGenerator());
                }
            }

        }
    }
}