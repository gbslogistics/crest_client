<?php

namespace GbsLogistics\Crest\Response;


use GbsLogistics\Crest\Http\ContentTypeParser;
use GbsLogistics\Crest\Json\IJsonParser;
use GbsLogistics\Crest\Response\Model\GenericResponse;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

/**
 * Does common response processing that all responses need.
 *
 * Class ResponsePreprocessor
 * @package GbsLogistics\Crest\Response
 */
class ResponsePreprocessor
{
    /** @var IJsonParser */
    private $jsonParser;

    /** @var ContentTypeParser */
    private $contentTypeParser;

    /**
     * ResponsePreprocessor constructor.
     * @param IJsonParser $jsonParser
     * @param ContentTypeParser $contentTypeParser
     */
    public function __construct(IJsonParser $jsonParser, ContentTypeParser $contentTypeParser)
    {
        $this->jsonParser = $jsonParser;
        $this->contentTypeParser = $contentTypeParser;
    }

    public function parse(ResponseInterface $rawResponse): GenericResponse
    {
        $contentType = $this->contentTypeParser->parse($rawResponse);
        $jsonData = $this->jsonParser->parse($rawResponse->getBody());

        return new GenericResponse($jsonData, $contentType);
    }
}