<?php

namespace GbsLogistics\Crest\Response;


use GbsLogistics\Crest\Domain\Root;
use GbsLogistics\Crest\Response\Model\IResponse;

class RootResponse implements IResponse
{
    const VERSION = 'application/vnd.ccp.eve.Api-v3+json';

    private $resources = [];

    /** @var Root */
    private $data;

    /**
     * RootResponse constructor.
     * @param array $resources
     * @param Root $data
     */
    public function __construct(array $resources, Root $data)
    {
        $this->resources = $resources;
        $this->data = $data;
    }

    public static function getVersion(): string
    {
        return self::VERSION;
    }

    public function getResource(string $path)
    {
        return $this->resources[$path] ?? null;
    }

    public function getData()
    {
        return $this->data;
    }
}