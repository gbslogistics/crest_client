<?php

namespace GbsLogistics\Crest\Response;
use GbsLogistics\Crest\Response\Model\PooledRequest;
use GbsLogistics\Crest\Response\Model\WrappedResponse;


/**
 * RequestPool stores information about requests to make to CREST, so they may be executed later.
 *
 * Class RequestStack
 * @package GbsLogistics\Crest\Response\Model
 */
class RequestPool
{
    /** @var PooledRequest[] */
    private $pool = [];

    /** @var array */
    private $responseObjects = [];

    /**
     * @param PooledRequest $request
     */
    public function addPooledRequest(PooledRequest $request)
    {
        $this->pool[] = $request;
    }

    /**
     * @return \Generator
     */
    public function getRequestsGenerator()
    {
        while ($request = array_shift($this->pool)) {
            yield $request;

        }

        return $this->responseObjects;
    }

    public function processResponse(WrappedResponse $wrappedResponse)
    {
        foreach ($wrappedResponse->getAdditionalRequests() as $additionalRequest) {
            $this->addPooledRequest($additionalRequest);
        }

        $responseObject = $wrappedResponse->getResponseObject();

        if (is_array($responseObject)) {
            array_push($this->responseObjects, ... $responseObject);
        } else {
            $this->responseObjects[] = $responseObject;
        }
    }
}