<?php

namespace GbsLogistics\Crest\Response\Model;


class PooledRequest
{
    /** @var string */
    private $href;

    /**
     * PooledRequest constructor.
     * @param string $href
     */
    public function __construct($href)
    {
        $this->href = $href;
    }

    /**
     * @return string
     */
    public function getHref()
    {
        return $this->href;
    }
}