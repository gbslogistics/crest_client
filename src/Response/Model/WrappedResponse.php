<?php

namespace GbsLogistics\Crest\Response\Model;


use GbsLogistics\Crest\Json\IJsonData;
use GbsLogistics\Crest\Proxy\Model\IHasProxies;
use GbsLogistics\Crest\Proxy\Proxy;
use Psr\Http\Message\ResponseInterface;

class WrappedResponse
{
    /** @var mixed */
    private $responseObject;

    /** @var PooledRequest[] */
    private $additionalRequests = [];

    /** @var IJsonData */
    private $jsonData;

    /**
     * WrappedResponse constructor.
     * @param mixed $responseObject
     * @param PooledRequest[] $additionalRequests
     * @param IJsonData $jsonData
     */
    public function __construct($responseObject, array $additionalRequests = [], IJsonData $jsonData = null)
    {
        $this->responseObject = $responseObject;
        $this->additionalRequests = $additionalRequests;
        $this->jsonData = $jsonData;
    }

    /**
     * @return mixed
     */
    public function getResponseObject()
    {
        return $this->responseObject;
    }

    /**
     * @return \Generator
     */
    public function getAdditionalRequests()
    {
        yield from $this->additionalRequests;

    }

    /**
     * @return IJsonData|null
     */
    public function getJsonData()
    {
        return $this->jsonData;
    }
}