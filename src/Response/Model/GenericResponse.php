<?php

namespace GbsLogistics\Crest\Response\Model;


use GbsLogistics\Crest\Http\Model\ContentType;
use GbsLogistics\Crest\Json\IJsonData;

class GenericResponse
{
    /** @var IJsonData */
    private $jsonData;

    /** @var ContentType */
    private $contentType;

    /**
     * GenericResponse constructor.
     * @param $jsonData
     * @param $contentType
     */
    public function __construct(IJsonData $jsonData, ContentType $contentType)
    {
        $this->jsonData = $jsonData;
        $this->contentType = $contentType;
    }

    /**
     * @return IJsonData
     */
    public function getJsonData()
    {
        return $this->jsonData;
    }

    /**
     * @return ContentType
     */
    public function getContentType()
    {
        return $this->contentType;
    }
}