<?php

namespace GbsLogistics\Crest;


use Concat\Http\Middleware\RateLimitProvider;
use GbsLogistics\Crest\Client\Model\IOptions;
use GbsLogistics\Crest\Cache\NullCacheItemPool;
use GbsLogistics\Crest\Http\CrestRateLimitProvider;
use GbsLogistics\Crest\Proxy\Strategy\EagerProxyStrategy;
use GbsLogistics\Crest\Proxy\Strategy\AbstractProxyStrategy;
use GbsLogistics\Crest\Proxy\Strategy\LazyProxyStrategy;
use GuzzleHttp\HandlerStack;
use Psr\Cache\CacheItemPoolInterface;

class OptionsBuilder
{
//    const EAGER_FETCH = 'eager';
//    const LAZY_FETCH = 'lazy';

    /** @var string */
    private $rootURI;

    /** @var int */
    private $connectTimeout = 0;

    /** @var string */
    private $userAgent = 'http://bitbucket.com/gbslogistics/crest_client , unknown user';

//    private $fetchMode = self::LAZY_FETCH;
    private $fetchMode;

    private $concurrencyLimit = 20;

    /** @var int */
    private $recursionLimit = 5;

    /** @var int */
    private $requestsPerSecond = 150;

    /** @var HandlerStack */
    private $handlerStack = null;

    /**
     * OptionsBuilder constructor.
     * @param string $rootURI
     */
    public function __construct($rootURI)
    {
        $this->rootURI = $rootURI;
        $this->cache = new NullCacheItemPool();
    }

    /**
     * Set the connection timeout, in seconds.
     *
     * @param int $connectTimeout
     * @return OptionsBuilder
     */
    public function setConnectTimeout($connectTimeout)
    {
        $this->connectTimeout = $connectTimeout;
        return $this;
    }

    /**
     * Set the user agent.
     *
     * @param string $userAgent
     * @return OptionsBuilder
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;
        return $this;
    }

    /**
     * Sets the desired fetch mode.
     *
     * @param string $fetchMode Can be OptionsBuilder::EAGER_FETCH or OptionsBuilder::LAZY_FETCH. If not set, defaults
     *     to lazy.
     * @throws \InvalidArgumentException Thrown if an illegal fetch mode is specified.
     */
//    public function setFetchMode(string $fetchMode)
//    {
//        if (!in_array($fetchMode, [self::EAGER_FETCH, self::LAZY_FETCH])) {
//            throw new \InvalidArgumentException(sprintf('Fetch mode must be either "%s" or "%s".', self::LAZY_FETCH, self::EAGER_FETCH));
//        }
//
//        $this->fetchMode = $fetchMode;
//    }

    /**
     * Sets how many simultaneous HTTP requests the client is allowed to make at once. Defaults to 20.
     *
     * @param int $concurrencyLimit
     */
    public function setConcurrencyLimit($concurrencyLimit)
    {
        $this->concurrencyLimit = $concurrencyLimit;
    }

    /**
     * @param CacheItemPoolInterface $cache
     */
    public function setCache(CacheItemPoolInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Defaults to 150 requests/second.
     *
     * @param int $requestsPerSecond
     */
    public function setRequestsPerSecond($requestsPerSecond)
    {
        $this->requestsPerSecond = $requestsPerSecond;
    }

    /**
     * When fetch mode is set to EAGER_FETCH, controls how deeply the client delves into the tree before switching all
     * subsequent fetches to lazy. Defaults to 5.
     *
     * NOTE: This does not include the initial call to the CREST root to discover endpoint URLs.
     *
     * @param int $recursionLimit
     */
    public function setRecursionLimit($recursionLimit)
    {
        $this->recursionLimit = $recursionLimit;
    }

    /**
     * @param HandlerStack $handlerStack
     */
    public function setHandlerStack(HandlerStack $handlerStack)
    {
        $this->handlerStack = $handlerStack;
    }

    public function build(): IOptions
    {
        $rootURI = $this->rootURI;
        $connectTimeout = $this->connectTimeout;
        $userAgent = $this->userAgent;
        $fetchMode = new LazyProxyStrategy();
        $concurrencyLimit = $this->concurrencyLimit;
        $rateLimitProvider = new CrestRateLimitProvider($this->requestsPerSecond);
        $handlerStack = $this->handlerStack;

        return new class(
            $rootURI,
            $connectTimeout,
            $userAgent,
            $fetchMode,
            $concurrencyLimit,
            $rateLimitProvider,
            $handlerStack
        ) implements IOptions {
            /** @var string */
            private $rootURI;

            /** @var int */
            private $connectTimeout;

            /** @var string */
            private $userAgent;

            /** @var AbstractProxyStrategy */
            private $proxyStrategy;

            /** @var int */
            private $concurrencyLimit;

            /** @var RateLimitProvider */
            private $rateLimitProvider;

            /** @var HandlerStack */
            private $handlerStack;

            /**
             * @param $rootURI
             * @param $connectTimeout
             * @param $userAgent
             * @param $proxyStrategy
             * @param $concurrencyLimit
             * @param $rateLimitProvider
             * @param $handlerStack
             */
            public function __construct($rootURI, $connectTimeout, $userAgent, $proxyStrategy, $concurrencyLimit, $rateLimitProvider, $handlerStack)
            {
                $this->rootURI = $rootURI;
                $this->connectTimeout = $connectTimeout;
                $this->userAgent = $userAgent;
                $this->proxyStrategy = $proxyStrategy;
                $this->concurrencyLimit = $concurrencyLimit;
                $this->rateLimitProvider = $rateLimitProvider;
                $this->handlerStack = $handlerStack;
            }

            public function getRootURI(): string
            {
                return $this->rootURI;
            }

            public function getConnectTimeout(): int
            {
                return $this->connectTimeout;
            }

            public function getUserAgent(): string
            {
                return $this->userAgent;
            }

            public function getProxyStrategy(): AbstractProxyStrategy
            {
                return $this->proxyStrategy;
            }

            public function getConcurrencyLimit(): int
            {
                return $this->concurrencyLimit;
            }

            public function getRateLimitProvider(): RateLimitProvider
            {
                return $this->rateLimitProvider;
            }

            /**
             * @return HandlerStack|null
             */
            public function getGuzzleHandlerStack()
            {
                return $this->handlerStack;
            }
        };
    }
}