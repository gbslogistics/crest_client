<?php

namespace GbsLogistics\Crest\Proxy\Strategy;


use GbsLogistics\Crest\Proxy\Model\IHasProxies;
use GbsLogistics\Crest\Proxy\Proxy;
use GbsLogistics\Crest\Proxy\Model\ResolvedObjectStorage;

abstract class AbstractProxyStrategy
{
    /** @var ResolvedObjectStorage */
    protected $storage = null;

    public function registerResolutionFunctions(callable $resolutionFunction, array $objects, int $recursionLevel = 0)
    {
        if (0 === $recursionLevel) {
            $this->storage = new ResolvedObjectStorage();
        }

        $proxies = array_reduce($objects, function ($carry, $item) {
            if (is_object($item) && $item instanceof IHasProxies) {
                $proxies = $item->getProxies();
                if (is_array($proxies) && count($proxies) > 0) {
                    array_push($carry, ...$proxies);
                }
            }

            return $carry;
        }, []);

        if (count($proxies) > 0) {
            $this->doRegisterResolutionFunctions($resolutionFunction, $proxies, $recursionLevel);
        }
    }

    /**
     * Registers a callback function on the proxies sent to it.
     *
     * @param callable $resolutionFunction
     * @param Proxy[] $proxies
     * @param int $recursionLevel
     */
    abstract protected function doRegisterResolutionFunctions(callable $resolutionFunction, array $proxies, int $recursionLevel);
}