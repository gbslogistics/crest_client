<?php

namespace GbsLogistics\Crest\Proxy\Strategy;


use GbsLogistics\Crest\Proxy\Proxy;
use GbsLogistics\Crest\Proxy\Model\LazyProxyResolver;
use GbsLogistics\Crest\Response\Model\WrappedResponse;

class EagerProxyStrategy extends AbstractProxyStrategy
{
    private $recursionLimit = 5;

    /**
     * EagerProxyStrategy constructor.
     * @param int $recursionLimit
     */
    public function __construct($recursionLimit)
    {
        $this->recursionLimit = $recursionLimit;
    }

    /**
     * @param callable $resolutionFunction
     * @param Proxy[] $proxies
     * @param int $recursionLevel
     */
    protected function doRegisterResolutionFunctions(callable $resolutionFunction, array $proxies, int $recursionLevel = 0)
    {
        $proxiesToResolve = [];
        $storage = $this->storage;

        foreach ($proxies as $proxy) {
            $href = $proxy->getHref();

            if ($recursionLevel >= $this->recursionLimit) {
                $proxy->setResolutionFunction(new LazyProxyResolver($href, $resolutionFunction));
            } else {
                if (is_string($href)) {
                    $proxy->setResolutionFunction(function () use ($href, $storage) {
                        return $storage->getItem($href);
                    });

                    if (!$storage->hasItem($href)) {
                        $proxiesToResolve[] = $proxy;
                    }
                } else {
                    /** @var string[] $href */
                    $proxy->setResolutionFunction(function () use ($href, $storage) {
                        return array_map(function ($item) use ($storage) {
                            return $storage->getItem($item);
                        }, $href);
                    });

                    foreach ($href as $item) {
                        if (!$storage->hasItem($item)) {
                            $proxiesToResolve[] = $proxy;
                        }
                    }
                }
            }
        }

        if (count($proxiesToResolve) > 0) {
            $hrefs = array_reduce($proxiesToResolve, function (array $carry, Proxy $proxy) {
                $hrefs = $proxy->getHref();
                if (is_array($hrefs)) {
                    array_push($carry, ...$hrefs);
                } else {
                    array_push($carry, $hrefs);
                }

                return $carry;
            }, []);

            $resolvedResponses = $resolutionFunction(...$hrefs);
            $resolvedObjects = [];

            /**
             * @var string $href
             * @var WrappedResponse $response
             */
            foreach ($resolvedResponses as $href => $response) {
                $responseObject = $response->getResponseObject();

                $storage->addItem($href, $responseObject);
                $resolvedObjects[] = $responseObject;
            }

            $this->registerResolutionFunctions($resolutionFunction, $resolvedObjects, $recursionLevel + 1);
        }
    }
}