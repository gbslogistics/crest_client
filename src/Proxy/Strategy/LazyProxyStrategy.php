<?php

namespace GbsLogistics\Crest\Proxy\Strategy;


use GbsLogistics\Crest\Proxy\Proxy;
use GbsLogistics\Crest\Proxy\Model\LazyProxyResolver;

/**
 * Uses a policy of loading proxies only when they're actually called for. NOTE: If a circular route occurs between
 * objects, LazyProxyStrategy WILL NOT avoid subsequent loads of the object in question.
 *
 * Class LazyProxyStrategy
 * @package GbsLogistics\Crest\Proxy\Strategy
 */
class LazyProxyStrategy extends AbstractProxyStrategy
{
    /**
     * Registers a callback function on the proxies sent to it.
     *
     * @param callable $resolutionFunction
     * @param Proxy[] $proxies
     * @param int $recursionLevel
     */
    protected function doRegisterResolutionFunctions(callable $resolutionFunction, array $proxies, int $recursionLevel)
    {
        foreach ($proxies as $proxy) {
            if (null !== $proxy && $proxy instanceof Proxy) {
                $proxy->setResolutionFunction(new LazyProxyResolver($proxy->getHref(), $resolutionFunction));
            }
        }
    }
}