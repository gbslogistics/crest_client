<?php

namespace GbsLogistics\Crest\Proxy;


class Proxy
{
    /** @var string|array */
    private $href;

    /** @var callable */
    private $resolutionFunction = null;

    /** @var mixed */
    private $resolvedItem = null;

    /** @var bool */
    private $isResolved = false;

    /**
     * Proxy constructor.
     * @param $href
     */
    public function __construct($href)
    {
        $throw = false;
        if (is_array($href)) {
            foreach ($href as $item) {
                if (!is_string($item)) {
                    $throw = true;
                    break;
                }
            }
        } elseif (!is_string($href)) {
            $throw = true;
        }

        if ($throw) {
            throw new \InvalidArgumentException('Argument passed to Proxy::__construct must be a string or an array of strings.');
        }

        $this->href = $href;
    }

    /**
     * @return string|array
     */
    public function getHref()
    {
        return $this->href;
    }

    /**
     * Callables set via this function take the following form:
     *
     * function resolve(): object
     *
     * This callable will be executed every time the proxy is resolved.
     *
     * @param callable $resolutionFunction
     */
    public function setResolutionFunction(callable $resolutionFunction)
    {
        $this->resolutionFunction = $resolutionFunction;
    }

    /**
     * Resolves the proxy, invoking the resolution function.
     *
     * @return mixed
     */
    public function resolve()
    {
        if (null === $this->resolutionFunction) {
            throw new \RuntimeException('No resolution function was specified for proxy.');
        }

        return call_user_func($this->resolutionFunction);
    }

    private function makeKey($key) {
        return sprintf('%s":"%s":private', $key, self::class);
    }

    function __debugInfo()
    {
        return [
            $this->makeKey('href') => $this->href,
            $this->makeKey('isResolved') => $this->isResolved,
            $this->makeKey('resolvedItem') => $this->resolvedItem,
            $this->makeKey('resolutionFunction') => null === $this->resolutionFunction ? null : 'object(Closure, truncated)'
        ];
    }


}