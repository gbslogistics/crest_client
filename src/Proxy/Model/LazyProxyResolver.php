<?php

namespace GbsLogistics\Crest\Proxy\Model;
use GbsLogistics\Crest\Proxy\Proxy;
use GbsLogistics\Crest\Response\Model\WrappedResponse;


/**
 * Simple helper to cache the results of a closure and return it on subsequent invocations of the closure.
 *
 * Class LazyProxyResolver
 * @package GbsLogistics\Crest\Proxy\Model
 */
class LazyProxyResolver
{
    /** @var string|array */
    private $href;

    /** @var callable */
    private $resolutionFunction;

    /** @var mixed */
    private $resolvedValue;

    /** @var bool */
    private $resolved = false;

    /**
     * @param string|array $href
     * @param callable $resolutionFunction
     */
    public function __construct($href, callable $resolutionFunction)
    {
        $this->href = $href;
        $this->resolutionFunction = $resolutionFunction;
    }

    public function __invoke()
    {
        if (!$this->resolved) {
            /** @var WrappedResponse|array $wrappedResponse */
            if (is_array($this->href)) {
                $wrappedResponse = call_user_func($this->resolutionFunction, ...$this->href);
            } else {
                $wrappedResponse = call_user_func($this->resolutionFunction, $this->href);
            }

            if (is_array($wrappedResponse)) {
                $this->resolvedValue = [];
                foreach ($wrappedResponse as $response) {
                    if (is_object($response) && $response instanceof WrappedResponse) {
                        array_push($this->resolvedValue, $response->getResponseObject());
                        $this->registerProxies($response->getResponseObject());
                    }
                }

                if (1 === count($this->resolvedValue)) {
                    $this->resolvedValue = $this->resolvedValue[0];
                }
            } elseif (is_object($wrappedResponse) && $wrappedResponse instanceof WrappedResponse) {
                $this->resolvedValue = $wrappedResponse->getResponseObject();
                $this->registerProxies($wrappedResponse->getResponseObject());
            }


            $this->resolved = true;
            $this->resolutionFunction = null;
        }

        return $this->resolvedValue;
    }

    private function registerProxies($objectOrArray)
    {
        if (is_array($objectOrArray)) {
            foreach ($objectOrArray as $object) {
                $this->registerProxiesOnObject($object);
            }
        } else {
            $this->registerProxiesOnObject($objectOrArray);
        }
    }

    private function registerProxiesOnObject($object)
    {
        if (is_object($object) && $object instanceof IHasProxies) {
            foreach ($object->getProxies() as $proxy) {
                if (null !== $proxy && $proxy instanceof Proxy) {
                    $proxy->setResolutionFunction(new self($proxy->getHref(), $this->resolutionFunction));
                }
            }
        }
    }
}