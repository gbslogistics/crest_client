<?php

namespace GbsLogistics\Crest\Proxy\Model;


/**
 * Holds objects that have already been resolved. Used by EagerProxyStrategy to avoid circular request loops.
 *
 * Class ResolvedObjectStorage
 * @package GbsLogistics\Crest\Proxy\Model
 */
class ResolvedObjectStorage
{
    /** @var mixed[] */
    private $items = [];

    public function addItem(string $href, $item)
    {
        $this->items[$href] = $item;
    }

    /**
     * @param string $href
     * @return mixed|null
     */
    public function getItem(string $href)
    {
        return $this->items[$href] ?? null;
    }

    /**
     * @param string $href
     * @return bool
     */
    public function hasItem(string $href)
    {
        return isset($this->items[$href]);
    }
}