<?php

namespace GbsLogistics\Crest\Proxy\Model;


use GbsLogistics\Crest\Proxy\Proxy;

interface IHasProxies
{
    /**
     * @return Proxy[]
     */
    public function getProxies(): array;
}