<?php

namespace GbsLogistics\Crest\DomainMapper\Mapper;


use GbsLogistics\Crest\Domain\MarketTypePriceCollectionMember;
use GbsLogistics\Crest\DomainMapper\IMapper;
use GbsLogistics\Crest\Json\IJsonData;

class MarketTypePriceCollectionMapper implements IMapper
{
    /**
     * Maps JSON data to a domain object.
     *
     * @param IJsonData $data
     * @return object
     */
    public function map(IJsonData $data)
    {
        $type = $data->getJsonDataByFirstLevelKey('type');

        return new MarketTypePriceCollectionMember(
            $type->getJsonDataByFirstLevelKey('id_str'),
            $type->getJsonDataByFirstLevelKey('name'),
            $data->getJsonDataByFirstLevelKey('adjustedPrice'),
            $data->getJsonDataByFirstLevelKey('averagePrice'),
            $type->getJsonDataByFirstLevelKey('href')
        );
    }

    /**
     * Returns the version for which this mapper is intended.
     *
     * @return int
     */
    public function getVersion(): int
    {
        return 1;
    }

    /**
     * Returns the name of the resource for which this mapper is intended.
     *
     * @return string
     */
    public function getResourceName(): string
    {
        return 'MarketTypePriceCollection';
    }
}