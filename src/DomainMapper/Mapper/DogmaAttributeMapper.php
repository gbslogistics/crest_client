<?php

namespace GbsLogistics\Crest\DomainMapper\Mapper;


use GbsLogistics\Crest\Domain\DogmaAttribute;
use GbsLogistics\Crest\DomainMapper\IMapper;
use GbsLogistics\Crest\Json\IJsonData;

class DogmaAttributeMapper implements IMapper
{
    /**
     * Maps JSON data to a domain object.
     *
     * @param IJsonData $data
     * @return object
     */
    public function map(IJsonData $data)
    {
        return new DogmaAttribute(
            $data->getJsonDataByFirstLevelKey('id_str'),
            $data->getJsonDataByFirstLevelKey('name'),
            $data->getJsonDataByFirstLevelKey('description'),
            $data->getJsonDataByFirstLevelKey('defaultValue'),
            $data->getJsonDataByFirstLevelKey('displayName'),
            $data->getJsonDataByFirstLevelKey('highIsGood'),
            $data->getJsonDataByFirstLevelKey('iconID'),
            $data->getJsonDataByFirstLevelKey('published'),
            $data->getJsonDataByFirstLevelKey('stackable'),
            $data->getJsonDataByFirstLevelKey('unitID')
        );
    }

    /**
     * Returns the version for which this mapper is intended.
     *
     * @return int
     */
    public function getVersion(): int
    {
        return 1;
    }

    /**
     * Returns the name of the resource for which this mapper is intended.
     *
     * @return string
     */
    public function getResourceName(): string
    {
        return 'DogmaAttribute';
    }
}