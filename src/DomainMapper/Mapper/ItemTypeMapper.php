<?php

namespace GbsLogistics\Crest\DomainMapper\Mapper;


use GbsLogistics\Crest\Domain\ItemType;
use GbsLogistics\Crest\Domain\Partial;
use GbsLogistics\Crest\Domain\Partial\PartialDogmaAttribute;
use GbsLogistics\Crest\DomainMapper\IMapper;
use GbsLogistics\Crest\Json\IJsonData;

class ItemTypeMapper implements IMapper
{
    /**
     * Maps JSON data to a domain object.
     *
     * @param IJsonData $data
     * @return object
     */
    public function map(IJsonData $data)
    {
        $partialDogmaAttributes = [];
        $dogma = $data->getJsonDataByFirstLevelKey('dogma');
        $attributes = $dogma->getJsonDataByFirstLevelKey('attributes');

        /**
         * @var int $index
         * @var IJsonData $dogmaItem
         */
        foreach ($attributes->getRootGenerator() as $index => $dogmaItem) {
            $attribute = $dogmaItem->getJsonDataByFirstLevelKey('attribute');
            $partialDogmaAttributes[] = new PartialDogmaAttribute(
                $attribute->getJsonDataByFirstLevelKey('id_str'),
                $attribute->getJsonDataByFirstLevelKey('href'),
                $attribute->getJsonDataByFirstLevelKey('name'),
                $dogmaItem->getJsonDataByFirstLevelKey('value')
            );
        }

        return new ItemType(
            $data->getJsonDataByFirstLevelKey('id_str'),
            $data->getJsonDataByFirstLevelKey('name'),
            $data->getJsonDataByFirstLevelKey('description'),
            $partialDogmaAttributes,
            // TODO: Implement dogma effects
            [],
            $data->getJsonDataByFirstLevelKey('mass'),
            $data->getJsonDataByFirstLevelKey('published'),
            $data->getJsonDataByFirstLevelKey('radius'),
            $data->getJsonDataByFirstLevelKey('volume'),
            $data->getJsonDataByFirstLevelKey('portionSize'),
            $data->getJsonDataByFirstLevelKey('capacity'),
            $data->getJsonDataByFirstLevelKey('iconID')
        );
    }

    /**
     * Returns the version for which this mapper is intended.
     *
     * @return int
     */
    public function getVersion(): int
    {
        return 3;
    }

    /**
     * Returns the name of the resource for which this mapper is intended.
     *
     * @return string
     */
    public function getResourceName(): string
    {
        return 'ItemType';
    }
}