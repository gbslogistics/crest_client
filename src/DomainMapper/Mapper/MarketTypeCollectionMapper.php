<?php

namespace GbsLogistics\Crest\DomainMapper\Mapper;


use GbsLogistics\Crest\Domain\MarketTypeCollectionMember;
use GbsLogistics\Crest\DomainMapper\IMapper;
use GbsLogistics\Crest\Json\IJsonData;

class MarketTypeCollectionMapper implements IMapper
{
    /**
     * Maps JSON data to a domain object.
     *
     * @param IJsonData $data
     * @return object
     */
    public function map(IJsonData $data)
    {
        $marketGroup = $data->getJsonDataByFirstLevelKey('marketGroup');
        $type = $data->getJsonDataByFirstLevelKey('type');
        $icon = $type->getJsonDataByFirstLevelKey('icon');

        return new MarketTypeCollectionMember(
            $data->getJsonDataByFirstLevelKey('id'),
            $type->getJsonDataByFirstLevelKey('name'),
            $icon->getJsonDataByFirstLevelKey('href'),
            $type->getJsonDataByFirstLevelKey('href'),
            $marketGroup->getJsonDataByFirstLevelKey('id'),
            $marketGroup->getJsonDataByFirstLevelKey('href')
        );
    }

    /**
     * Returns the version for which this mapper is intended.
     *
     * @return int
     */
    public function getVersion(): int
    {
        return 1;
    }

    /**
     * Returns the name of the resource for which this mapper is intended.
     *
     * @return string
     */
    public function getResourceName(): string
    {
        return 'MarketTypeCollection';
    }
}