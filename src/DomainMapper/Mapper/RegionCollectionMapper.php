<?php

namespace GbsLogistics\Crest\DomainMapper\Mapper;


use GbsLogistics\Crest\Domain\RegionCollectionMember;
use GbsLogistics\Crest\DomainMapper\IMapper;
use GbsLogistics\Crest\Json\IJsonData;

class RegionCollectionMapper implements IMapper
{

    /**
     * Maps JSON data to a domain object.
     *
     * @param IJsonData $data
     * @return object
     */
    public function map(IJsonData $data)
    {
        $array = $data->getRootArray();
        return new RegionCollectionMember($array['id_str'], $array['name'], $array['href']);
    }

    /**
     * Returns the version for which this mapper is intended.
     *
     * @return int
     */
    public function getVersion(): int
    {
        return 1;
    }

    /**
     * Returns the name of the resource for which this mapper is intended.
     *
     * @return string
     */
    public function getResourceName(): string
    {
        return 'RegionCollection';
    }
}