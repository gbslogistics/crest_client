<?php

namespace GbsLogistics\Crest\DomainMapper\Mapper;


use GbsLogistics\Crest\Domain\Constellation;
use GbsLogistics\Crest\DomainMapper\IMapper;
use GbsLogistics\Crest\Json\IJsonData;

class ConstellationMapper implements IMapper
{
    /**
     * Maps JSON data to a domain object.
     *
     * @param IJsonData $data
     * @return object
     */
    public function map(IJsonData $data)
    {
        $array = $data->getRootArray();
        $position = $data->getJsonDataByFirstLevelKey('position');
        $region = $data->getJsonDataByFirstLevelKey('region');
        $positionX = $positionY = $positionZ = null;
        if ($position instanceof IJsonData) {
            $positionX = $position->getJsonDataByFirstLevelKey('x');
            $positionY = $position->getJsonDataByFirstLevelKey('y');
            $positionZ = $position->getJsonDataByFirstLevelKey('z');
        }

        return new Constellation(
            $array['name'],
            $positionX,
            $positionY,
            $positionZ,
            $region->getJsonDataByFirstLevelKey('href'),
            // TODO: Implement systems
            []
        );
    }

    /**
     * Returns the version for which this mapper is intended.
     *
     * @return int
     */
    public function getVersion(): int
    {
        return 1;
    }

    /**
     * Returns the name of the resource for which this mapper is intended.
     *
     * @return string
     */
    public function getResourceName(): string
    {
        return 'Constellation';
    }
}