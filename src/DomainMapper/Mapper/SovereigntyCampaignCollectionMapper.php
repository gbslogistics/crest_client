<?php

namespace GbsLogistics\Crest\DomainMapper\Mapper;


use GbsLogistics\Crest\Domain\Partial\PartialAlliance;
use GbsLogistics\Crest\Domain\Partial\PartialSolarSystem;
use GbsLogistics\Crest\Domain\SovereigntyCampaign\Attackers;
use GbsLogistics\Crest\Domain\SovereigntyCampaign\Defender;
use GbsLogistics\Crest\Domain\SovereigntyCampaign\FreeportContestant;
use GbsLogistics\Crest\Domain\SovereigntyCampaignCollectionMember;
use GbsLogistics\Crest\DomainMapper\IMapper;
use GbsLogistics\Crest\Json\IJsonData;

class SovereigntyCampaignCollectionMapper implements IMapper
{

    /**
     * Maps JSON data to a domain object.
     *
     * @param IJsonData $data
     * @return object
     */
    public function map(IJsonData $data)
    {
        $solarSystem = $data->getJsonDataByFirstLevelKey('sourceSolarsystem');
        $eventType = $data->getJsonDataByFirstLevelKey('eventType');
        $defender = null;
        $antagonists = null;

        if (SovereigntyCampaignCollectionMember::FREEPORT == $eventType) {
            $antagonists = [];
            $scores = $data->getJsonDataByFirstLevelKey('scores');

            if (null !== $scores) {
                /** @var IJsonData $antagonist */
                foreach ($scores->getRootGenerator() as $antagonist) {
                    $antagonists[] = new FreeportContestant(
                        $antagonist->getJsonDataByFirstLevelKey('score'),
                        $this->createPartialAlliance($antagonist->getJsonDataByFirstLevelKey('team'))
                    );
                }
            }
        } else {
            $defenderJson = $data->getJsonDataByFirstLevelKey('defender');
            $defender = new Defender(
                $defenderJson->getJsonDataByFirstLevelKey('score'),
                $this->createPartialAlliance($defenderJson->getJsonDataByFirstLevelKey('defender'))
            );

            $attackerJson = $data->getJsonDataByFirstLevelKey('attackers');
            $antagonists = new Attackers($attackerJson->getJsonDataByFirstLevelKey('score'));
        }

        return new SovereigntyCampaignCollectionMember(
            $data->getJsonDataByFirstLevelKey('campaignID_str'),
            $eventType,
            $data->getJsonDataByFirstLevelKey('sourceItemID_str'),
            new \DateTime($data->getJsonDataByFirstLevelKey('startTime'), new \DateTimeZone('UTC')),
            new PartialSolarSystem(
                $solarSystem->getJsonDataByFirstLevelKey('id_str'),
                $solarSystem->getJsonDataByFirstLevelKey('name'),
                $solarSystem->getJsonDataByFirstLevelKey('href')
            ),
            $defender,
            $antagonists
        );
    }

    /**
     * Returns the version for which this mapper is intended.
     *
     * @return int
     */
    public function getVersion(): int
    {
        return 1;
    }

    /**
     * Returns the name of the resource for which this mapper is intended.
     *
     * @return string
     */
    public function getResourceName(): string
    {
        return 'SovCampaignsCollection';
    }

    /**
     * @param IJsonData $team
     * @return PartialAlliance
     */
    private function createPartialAlliance(IJsonData $team)
    {
        return new PartialAlliance(
            $team->getJsonDataByFirstLevelKey('id_str'),
            $team->getJsonDataByFirstLevelKey('name'),
            $team->getJsonDataByFirstLevelKey('href')
        );
    }
}