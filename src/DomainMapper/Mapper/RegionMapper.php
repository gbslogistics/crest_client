<?php

namespace GbsLogistics\Crest\DomainMapper\Mapper;


use GbsLogistics\Crest\Domain\Region;
use GbsLogistics\Crest\DomainMapper\IMapper;
use GbsLogistics\Crest\Json\IJsonData;

class RegionMapper implements IMapper
{
    /**
     * Maps JSON data to a domain object.
     *
     * @param IJsonData $data
     * @return object
     */
    public function map(IJsonData $data)
    {
        $array = $data->getRootArray();
        $constellationHrefs = [];

        $constellations = $data->getJsonDataByFirstLevelKey('constellations');

        /**
         * @var int $index
         * @var IJsonData $constellation
         */
        foreach ($constellations->getRootGenerator() as $index => $constellation) {
            $constellationHrefs[] = $constellation->getJsonDataByFirstLevelKey('href');
        }

        return new Region(
            $array['id_str'],
            $array['name'],
            $array['description'],
            $constellationHrefs
        );
    }

    /**
     * Returns the version for which this mapper is intended.
     *
     * @return int
     */
    public function getVersion(): int
    {
        return 1;
    }

    /**
     * Returns the name of the resource for which this mapper is intended.
     *
     * @return string
     */
    public function getResourceName(): string
    {
        return 'Region';
    }
}