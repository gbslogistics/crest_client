<?php

namespace GbsLogistics\Crest\DomainMapper\Mapper;


use GbsLogistics\Crest\Domain\MarketGroupCollectionMember;
use GbsLogistics\Crest\DomainMapper\IMapper;
use GbsLogistics\Crest\Json\IJsonData;

class MarketGroupCollectionMapper implements IMapper
{
    /**
     * Maps JSON data to a domain object.
     *
     * @param IJsonData $data
     * @return object
     */
    public function map(IJsonData $data)
    {
        $types = $data->getJsonDataByFirstLevelKey('types');
        $parentGroup = $data->getJsonDataByFirstLevelKey('parentGroup');
        return new MarketGroupCollectionMember(
            $data->getJsonDataByFirstLevelKey('id_str'),
            $data->getJsonDataByFirstLevelKey('name'),
            $data->getJsonDataByFirstLevelKey('description'),

            $data->getJsonDataByFirstLevelKey('href'),
            $types->getJsonDataByFirstLevelKey('href'),
            null === $parentGroup ? null : $parentGroup->getJsonDataByFirstLevelKey('href')
        );
    }

    /**
     * Returns the version for which this mapper is intended.
     *
     * @return int
     */
    public function getVersion(): int
    {
        return 1;
    }

    /**
     * Returns the name of the resource for which this mapper is intended.
     *
     * @return string
     */
    public function getResourceName(): string
    {
        return 'MarketGroupCollection';
    }
}