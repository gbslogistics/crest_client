<?php

namespace GbsLogistics\Crest\DomainMapper\Mapper;


use GbsLogistics\Crest\Domain\Partial\PartialAlliance;
use GbsLogistics\Crest\Domain\Partial\PartialItemType;
use GbsLogistics\Crest\Domain\Partial\PartialSolarSystem;
use GbsLogistics\Crest\Domain\SovereigntyStructureCollectionMember;
use GbsLogistics\Crest\DomainMapper\IMapper;
use GbsLogistics\Crest\Json\IJsonData;

class SovereigntyStructureCollectionMapper implements IMapper
{
    /**
     * Maps JSON data to a domain object.
     *
     * @param IJsonData $data
     * @return object
     */
    public function map(IJsonData $data)
    {
        $alliance = $data->getJsonDataByFirstLevelKey('alliance');
        $solarSystem = $data->getJsonDataByFirstLevelKey('solarSystem');
        $type = $data->getJsonDataByFirstLevelKey('type');
        $timezone = new \DateTimeZone('UTC');

        $vulnerableStartTime = $data->getJsonDataByFirstLevelKey('vulnerableStartTime');
        $vulnerableEndTime = $data->getJsonDataByFirstLevelKey('vulnerableEndTime');
        
        return new SovereigntyStructureCollectionMember(
            $data->getJsonDataByFirstLevelKey('vulnerabilityOccupancyLevel'),
            $data->getJsonDataByFirstLevelKey('structureID_str'),
            $vulnerableStartTime ? new \DateTime($vulnerableStartTime, $timezone) : null,
            $vulnerableEndTime ? new \DateTime($vulnerableEndTime, $timezone) : null,
            new PartialSolarSystem(
                $solarSystem->getJsonDataByFirstLevelKey('id_str'),
                $solarSystem->getJsonDataByFirstLevelKey('name'),
                $solarSystem->getJsonDataByFirstLevelKey('href')
            ),
            new PartialItemType(
                $type->getJsonDataByFirstLevelKey('id_str'),
                $type->getJsonDataByFirstLevelKey('name'),
                $type->getJsonDataByFirstLevelKey('href')
            ),
            new PartialAlliance(
                $alliance->getJsonDataByFirstLevelKey('id_str'),
                $alliance->getJsonDataByFirstLevelKey('name'),
                $alliance->getJsonDataByFirstLevelKey('href')
            )
        );
    }

    /**
     * Returns the version for which this mapper is intended.
     *
     * @return int
     */
    public function getVersion(): int
    {
        return 1;
    }

    /**
     * Returns the name of the resource for which this mapper is intended.
     *
     * @return string
     */
    public function getResourceName(): string
    {
        return 'SovStructureCollection';
    }
}