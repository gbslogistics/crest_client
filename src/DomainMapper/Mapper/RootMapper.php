<?php

namespace GbsLogistics\Crest\DomainMapper\Mapper;


use GbsLogistics\Crest\Domain\Root;
use GbsLogistics\Crest\DomainMapper\IMapper;
use GbsLogistics\Crest\Json\IJsonData;

class RootMapper implements IMapper
{
    /**
     * Maps JSON data to a domain object.
     *
     * @param IJsonData $data
     * @return object
     */
    public function map(IJsonData $data)
    {
        $array = $data->getRootArray();
        $dustUserCount = 0;
        $eveUserCount = $array['userCount'];
        $serverVersion = $array['serverVersion'];
        $dustStatus = 'offline';
        $eveStatus = $array['serviceStatus'];
        $serverStatus = null;
        $serverName = $array['serverName'];

        return new Root(
            $dustUserCount,
            $eveUserCount,
            $serverVersion,
            $dustStatus,
            $eveStatus,
            $serverStatus,
            $serverName
        );
    }

    /**
     * Returns the version for which this mapper is intended.
     *
     * @return int
     */
    public function getVersion(): int
    {
        return 5;
    }

    /**
     * Returns the name of the resource for which this mapper is intended.
     *
     * @return string
     */
    public function getResourceName(): string
    {
        return 'Api';
    }
}