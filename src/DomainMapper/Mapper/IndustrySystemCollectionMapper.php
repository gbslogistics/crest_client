<?php

namespace GbsLogistics\Crest\DomainMapper\Mapper;


use GbsLogistics\Crest\Domain\IndustrySystemCollectionMember;
use GbsLogistics\Crest\Domain\Partial\PartialSolarSystem;
use GbsLogistics\Crest\Domain\Partial\SystemCostIndex;
use GbsLogistics\Crest\DomainMapper\IMapper;
use GbsLogistics\Crest\Json\IJsonData;

class IndustrySystemCollectionMapper implements IMapper
{
    /**
     * Maps JSON data to a domain object.
     *
     * @param IJsonData $data
     * @return object
     */
    public function map(IJsonData $data)
    {
        $solarSystem = $data->getJsonDataByFirstLevelKey('solarSystem');
        $costIndices = $data->getJsonDataByFirstLevelKey('systemCostIndices');

        $indexObjects = [];
        /** @var IJsonData $costIndexData */
        foreach ($costIndices->getRootGenerator() as $costIndexData) {
            $indexObjects[] = new SystemCostIndex(
                $costIndexData->getJsonDataByFirstLevelKey('activityID_str'),
                $costIndexData->getJsonDataByFirstLevelKey('activityName'),
                $costIndexData->getJsonDataByFirstLevelKey('costIndex')
            );
        }

        return new IndustrySystemCollectionMember(
            new PartialSolarSystem(
                $solarSystem->getJsonDataByFirstLevelKey('id_str'),
                $solarSystem->getJsonDataByFirstLevelKey('name'),
                $solarSystem->getJsonDataByFirstLevelKey('href')
            ),
            $indexObjects
        );
    }

    /**
     * Returns the version for which this mapper is intended.
     *
     * @return int
     */
    public function getVersion(): int
    {
        return 1;
    }

    /**
     * Returns the name of the resource for which this mapper is intended.
     *
     * @return string
     */
    public function getResourceName(): string
    {
        return 'IndustrySystemCollection';
    }
}