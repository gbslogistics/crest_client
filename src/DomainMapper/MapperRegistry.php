<?php

namespace GbsLogistics\Crest\DomainMapper;


use GbsLogistics\Crest\DomainMapper\IMapper;
use GbsLogistics\Crest\Http\Model\ContentType;

class MapperRegistry
{
    /** @var IMapper[] */
    private $mappers = [];

    /**
     * Registers a mapper.
     *
     * @param IMapper $mapper
     */
    public function registerMapper(IMapper $mapper)
    {
        $this->mappers[$mapper->getResourceName()] = $mapper;
    }

    /**
     * Returns a registered mapper matching this resource name.
     *
     * @param string $resourceName
     * @return IMapper|null
     */
    public function getMapperByResourceName(string $resourceName)
    {
        return $this->mappers[$resourceName] ?? null;
    }

    /**
     * Returns a registered mapper corresponding to a specific resource name and version pair. Will return null if a
     * mapper exists for the resource name, but the version is incorrect.
     *
     * @param ContentType $contentType
     * @return IMapper|null
     */
    public function getMapperByContentType(ContentType $contentType)
    {
        $mapper = $this->getMapperByResourceName($contentType->getResourceName());

        if (null !== $mapper) {
            /** @var IMapper $mapper */
            return $contentType->getVersion() == $mapper->getVersion() ? $mapper : null;
        }

        return null;
    }
}