<?php

namespace GbsLogistics\Crest\DomainMapper;


use GbsLogistics\Crest\DomainMapper\Mapper\ConstellationMapper;
use GbsLogistics\Crest\DomainMapper\Mapper\DogmaAttributeMapper;
use GbsLogistics\Crest\DomainMapper\Mapper\IndustrySystemCollectionMapper;
use GbsLogistics\Crest\DomainMapper\Mapper\ItemTypeCollectionMapper;
use GbsLogistics\Crest\DomainMapper\Mapper\ItemTypeMapper;
use GbsLogistics\Crest\DomainMapper\Mapper\MarketGroupCollectionMapper;
use GbsLogistics\Crest\DomainMapper\Mapper\MarketGroupMapper;
use GbsLogistics\Crest\DomainMapper\Mapper\MarketTypeCollectionMapper;
use GbsLogistics\Crest\DomainMapper\Mapper\MarketTypePriceCollectionMapper;
use GbsLogistics\Crest\DomainMapper\Mapper\RegionCollectionMapper;
use GbsLogistics\Crest\DomainMapper\Mapper\RegionMapper;
use GbsLogistics\Crest\DomainMapper\Mapper\RootMapper;
use GbsLogistics\Crest\DomainMapper\Mapper\SovereigntyCampaignCollectionMapper;
use GbsLogistics\Crest\DomainMapper\Mapper\SovereigntyStructureCollectionMapper;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * The DomainMapper's responsibility is to translate responses received from CREST into Domain objects.
 *
 * Class DomainMapperProvider
 * @package GbsLogistics\Crest\DomainMapper
 */
class DomainMapperProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        $pimple[MapperRegistry::class] = function ($c) {
            $registry = new MapperRegistry();
            $registry->registerMapper(new ConstellationMapper());
            $registry->registerMapper(new DogmaAttributeMapper());
            $registry->registerMapper(new IndustrySystemCollectionMapper());
            $registry->registerMapper(new ItemTypeCollectionMapper());
            $registry->registerMapper(new ItemTypeMapper());
            $registry->registerMapper(new MarketGroupCollectionMapper());
            $registry->registerMapper(new MarketGroupMapper());
            $registry->registerMapper(new MarketTypeCollectionMapper());
            $registry->registerMapper(new MarketTypePriceCollectionMapper());
            $registry->registerMapper(new RegionCollectionMapper());
            $registry->registerMapper(new RegionMapper());
            $registry->registerMapper(new RootMapper());
            $registry->registerMapper(new SovereigntyStructureCollectionMapper());
            $registry->registerMapper(new SovereigntyCampaignCollectionMapper());

            return $registry;
        };
    }
}