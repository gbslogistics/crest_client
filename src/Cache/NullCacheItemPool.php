<?php

namespace GbsLogistics\Crest\Cache;


use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Cache\InvalidArgumentException;

class NullCacheItemPool implements CacheItemPoolInterface
{

    /**
     * Returns a Cache Item representing the specified key.
     *
     * This method must always return a CacheItemInterface object, even in case of
     * a cache miss. It MUST NOT return null.
     *
     * @param string $key
     *   The key for which to return the corresponding Cache Item.
     *
     * @throws InvalidArgumentException
     *   If the $key string is not a legal value a \Psr\Cache\InvalidArgumentException
     *   MUST be thrown.
     *
     * @return CacheItemInterface
     *   The corresponding Cache Item.
     */
    public function getItem($key)
    {
        return new class($key) implements CacheItemInterface {
            /**
             * @var string
             */
            private $key;

            /**
             *  constructor.
             */
            public function __construct($key)
            {
                $this->key = $key;
            }

            /**
             * Returns the key for the current cache item.
             *
             * The key is loaded by the Implementing Library, but should be available to
             * the higher level callers when needed.
             *
             * @return string
             *   The key string for this cache item.
             */
            public function getKey()
            {
                return $this->key;
            }

            /**
             * Retrieves the value of the item from the cache associated with this object's key.
             *
             * The value returned must be identical to the value originally stored by set().
             *
             * If isHit() returns false, this method MUST return null. Note that null
             * is a legitimate cached value, so the isHit() method SHOULD be used to
             * differentiate between "null value was found" and "no value was found."
             *
             * @return mixed
             *   The value corresponding to this cache item's key, or null if not found.
             */
            public function get()
            {
                return null;
            }

            /**
             * Confirms if the cache item lookup resulted in a cache hit.
             *
             * Note: This method MUST NOT have a race condition between calling isHit()
             * and calling get().
             *
             * @return bool
             *   True if the request resulted in a cache hit. False otherwise.
             */
            public function isHit()
            {
                return false;
            }

            /**
             * Sets the value represented by this cache item.
             *
             * The $value argument may be any item that can be serialized by PHP,
             * although the method of serialization is left up to the Implementing
             * Library.
             *
             * @param mixed $value
             *   The serializable value to be stored.
             *
             * @return static
             *   The invoked object.
             */
            public function set($value)
            {
                ;
            }

            /**
             * Sets the expiration time for this cache item.
             *
             * @param \DateTimeInterface $expiration
             *   The point in time after which the item MUST be considered expired.
             *   If null is passed explicitly, a default value MAY be used. If none is set,
             *   the value should be stored permanently or for as long as the
             *   implementation allows.
             *
             * @return static
             *   The called object.
             */
            public function expiresAt($expiration)
            {
                ;
            }

            /**
             * Sets the expiration time for this cache item.
             *
             * @param int|\DateInterval $time
             *   The period of time from the present after which the item MUST be considered
             *   expired. An integer parameter is understood to be the time in seconds until
             *   expiration. If null is passed explicitly, a default value MAY be used.
             *   If none is set, the value should be stored permanently or for as long as the
             *   implementation allows.
             *
             * @return static
             *   The called object.
             */
            public function expiresAfter($time)
            {
                ;
            }
        };
    }

    /**
     * Returns a traversable set of cache items.
     *
     * @param array $keys
     * An indexed array of keys of items to retrieve.
     *
     * @throws InvalidArgumentException
     *   If any of the keys in $keys are not a legal value a \Psr\Cache\InvalidArgumentException
     *   MUST be thrown.
     *
     * @return array|\Traversable
     *   A traversable collection of Cache Items keyed by the cache keys of
     *   each item. A Cache item will be returned for each key, even if that
     *   key is not found. However, if no keys are specified then an empty
     *   traversable MUST be returned instead.
     */
    public function getItems(array $keys = array())
    {
        return [];
    }

    /**
     * Confirms if the cache contains specified cache item.
     *
     * Note: This method MAY avoid retrieving the cached value for performance reasons.
     * This could result in a race condition with CacheItemInterface::get(). To avoid
     * such situation use CacheItemInterface::isHit() instead.
     *
     * @param string $key
     *    The key for which to check existence.
     *
     * @throws InvalidArgumentException
     *   If the $key string is not a legal value a \Psr\Cache\InvalidArgumentException
     *   MUST be thrown.
     *
     * @return bool
     *  True if item exists in the cache, false otherwise.
     */
    public function hasItem($key)
    {
        return false;
    }

    /**
     * Deletes all items in the pool.
     *
     * @return bool
     *   True if the pool was successfully cleared. False if there was an error.
     */
    public function clear()
    {
        return true;
    }

    /**
     * Removes the item from the pool.
     *
     * @param string $key
     *   The key for which to delete
     *
     * @throws InvalidArgumentException
     *   If the $key string is not a legal value a \Psr\Cache\InvalidArgumentException
     *   MUST be thrown.
     *
     * @return bool
     *   True if the item was successfully removed. False if there was an error.
     */
    public function deleteItem($key)
    {
        return true;
    }

    /**
     * Removes multiple items from the pool.
     *
     * @param array $keys
     *   An array of keys that should be removed from the pool.
     * @throws InvalidArgumentException
     *   If any of the keys in $keys are not a legal value a \Psr\Cache\InvalidArgumentException
     *   MUST be thrown.
     *
     * @return bool
     *   True if the items were successfully removed. False if there was an error.
     */
    public function deleteItems(array $keys)
    {
        return true;
    }

    /**
     * Persists a cache item immediately.
     *
     * @param CacheItemInterface $item
     *   The cache item to save.
     *
     * @return bool
     *   True if the item was successfully persisted. False if there was an error.
     */
    public function save(CacheItemInterface $item)
    {
        return true;
    }

    /**
     * Sets a cache item to be persisted later.
     *
     * @param CacheItemInterface $item
     *   The cache item to save.
     *
     * @return bool
     *   False if the item could not be queued or if a commit was attempted and failed. True otherwise.
     */
    public function saveDeferred(CacheItemInterface $item)
    {
        return true;
    }

    /**
     * Persists any deferred cache items.
     *
     * @return bool
     *   True if all not-yet-saved items were successfully saved or there were none. False otherwise.
     */
    public function commit()
    {
        return true;
    }
}