<?php

namespace GbsLogistics\Crest\Request;


use GbsLogistics\Crest\Request\Model\IRequest;

class MarketGroupRequest implements IRequest
{
    /**
     * Returns the path associated with this request.
     *
     * @return string
     */
    public function getPath(): string
    {
        return 'marketGroups';
    }
}
