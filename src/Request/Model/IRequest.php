<?php

namespace GbsLogistics\Crest\Request\Model;


interface IRequest
{
    /**
     * Returns the path associated with this request.
     *
     * @return string
     */
    public function getPath(): string;
}