<?php

namespace GbsLogistics\Crest\DependencyInjection;


use GbsLogistics\Crest\Client\ClientProvider;
use GbsLogistics\Crest\DomainMapper\DomainMapperProvider;
use GbsLogistics\Crest\Http\HttpProvider;
use GbsLogistics\Crest\Json\JsonProvider;
use GbsLogistics\Crest\Response\Factory\Registry;
use GbsLogistics\Crest\Response\Model\IRegisterable;
use GbsLogistics\Crest\Response\ResponseProvider;
use Pimple\Container;

class ContainerFactory
{
    /** @var Container */
    private static $container = null;

    public static function getContainer(): Container
    {
        if (null === self::$container) {
            self::$container = new Container();

            self::$container->register(new ClientProvider());
            self::$container->register(new DomainMapperProvider());
            self::$container->register(new HttpProvider());
            self::$container->register(new JsonProvider());
            self::$container->register(new ResponseProvider());
        }

        return self::$container;
    }
}