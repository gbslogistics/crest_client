<?php

namespace GbsLogistics\Crest\Domain\SovereigntyCampaign;


use GbsLogistics\Crest\Domain\Partial\PartialAlliance;

class BaseSide
{
    /** @var PartialAlliance */
    private $alliance = null;

    /** @var float */
    private $score;

    /**
     * BaseSide constructor.
     * @param float $score
     * @param PartialAlliance $alliance
     */
    public function __construct($score, PartialAlliance $alliance = null)
    {
        $this->score = $score;
        $this->alliance = $alliance;
    }

    /**
     * @return PartialAlliance
     */
    public function getAlliance()
    {
        return $this->alliance;
    }

    /**
     * @return float
     */
    public function getScore()
    {
        return $this->score;
    }
}