<?php

namespace GbsLogistics\Crest\Domain\SovereigntyCampaign;


use GbsLogistics\Crest\Domain\Partial\PartialAlliance;

class Attackers extends BaseSide
{
    public function __construct($score)
    {
        parent::__construct($score, null);
    }

    public function getAlliance()
    {
        throw new \RuntimeException('Attackers have no alliance information available.');
    }

}