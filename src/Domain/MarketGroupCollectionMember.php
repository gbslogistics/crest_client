<?php

namespace GbsLogistics\Crest\Domain;


use GbsLogistics\Crest\Proxy\Model\IHasProxies;
use GbsLogistics\Crest\Proxy\Proxy;

class MarketGroupCollectionMember implements IHasProxies
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $description;

    /** @var Proxy */
    private $types;

    /** @var Proxy */
    private $marketGroup;

    /** @var Proxy */
    private $parentMarketGroup;

    /**
     * MarketGroupCollectionMember constructor.
     * @param string $id
     * @param string $name
     * @param string $description
     * @param string $marketGroupHref
     * @param string $typesHref
     * @param null $parentMarketGroupHref
     */
    public function __construct(
        $id,
        $name,
        $description,
        string $marketGroupHref,
        string $typesHref,
        $parentMarketGroupHref = null
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->marketGroup = new Proxy($marketGroupHref);
        $this->types = new Proxy($typesHref);
        $this->parentMarketGroup =
            null === $parentMarketGroupHref
                ? null
                : new Proxy($parentMarketGroupHref);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return MarketTypeCollectionMember[]
     */
    public function getTypes()
    {
        return $this->types->resolve();
    }

    /**
     * @return MarketGroup
     */
    public function getMarketGroup()
    {
        return $this->marketGroup->resolve();
    }

    /**
     * @return Proxy[]
     */
    public function getProxies(): array
    {
        $proxies = [ $this->types, $this->marketGroup, $this->parentMarketGroup ];

        if (null !== $this->parentMarketGroup) {
            $proxies[] = $this->parentMarketGroup;
        }

        return $proxies;
    }

    /**
     * @return MarketGroup|null
     */
    public function getParentMarketGroup()
    {
        return null === $this->parentMarketGroup ? null : $this->parentMarketGroup->resolve();
    }

    public function isDust()
    {
        return (int)$this->getId() > 35000;
    }
}