<?php

namespace GbsLogistics\Crest\Domain;


use GbsLogistics\Crest\Proxy\Model\IHasProxies;
use GbsLogistics\Crest\Proxy\Proxy;

class RegionCollectionMember implements IHasProxies
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /** @var mixed */
    private $region;

    /**
     * RegionCollectionMember constructor.
     * @param string $id
     * @param string $name
     * @param mixed $region
     */
    public function __construct(string $id, string $name, $region)
    {
        $this->id = $id;
        $this->name = $name;
        $this->region = new Proxy($region);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region->resolve();
    }

    /**
     * @return Proxy[]
     */
    public function getProxies(): array
    {
        return [ $this->region ];
    }
}