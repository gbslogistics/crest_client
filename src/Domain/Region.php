<?php

namespace GbsLogistics\Crest\Domain;
use GbsLogistics\Crest\Proxy\Model\IHasProxies;
use GbsLogistics\Crest\Proxy\Proxy;


/**
 * Class Region
 * @package GbsLogistics\Crest\Domain
 * @todo Market order support
 */
class Region implements IHasProxies
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $description;

    /** @var array */
    private $constellations = [];

    /**
     * Region constructor.
     * @param string $id
     * @param string $name
     * @param string $description
     * @param array $constellations
     */
    public function __construct($id, $name, $description, array $constellations)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->constellations = new Proxy($constellations);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return array
     */
    public function getConstellations()
    {
        return $this->constellations->resolve();
    }

    /**
     * @return Proxy[]
     */
    public function getProxies(): array
    {
        return [$this->constellations];
    }
}