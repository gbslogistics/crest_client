<?php

namespace GbsLogistics\Crest\Domain;


use GbsLogistics\Crest\Domain\Partial\PartialDogmaAttribute;
use GbsLogistics\Crest\Proxy\Model\IHasProxies;
use GbsLogistics\Crest\Proxy\Proxy;

class ItemType implements IHasProxies
{
    // Required
    /** @var string */
    private $id;
    /** @var string */
    private $name;
    /** @var string */
    private $description;

    /** @var PartialDogmaAttribute[] */
    private $dogmaAttributes;

    // Optional

    /** @var int */
    private $mass;

    /** @var bool */
    private $published;

    /** @var int */
    private $radius;

    /** @var float */
    private $volume;

    /** @var int */
    private $portionSize;

    /** @var float */
    private $capacity;

    /** @var int */
    private $iconID;

    /**
     * ItemType constructor.
     * @param string $id
     * @param string $name
     * @param string $description
     * @param PartialDogmaAttribute[] $dogmaAttributes
     * @param array $dogmaEffects
     * @param int $mass
     * @param bool $published
     * @param int $radius
     * @param float $volume
     * @param int $portionSize
     * @param float $capacity
     * @param null $iconID
     */
    public function __construct(
        $id,
        $name,
        $description,
        $dogmaAttributes,

        // TODO: Implement dogma effects
        $dogmaEffects = [],

        $mass = null,
        $published = null,
        $radius = null,
        $volume = null,
        $portionSize = null,
        $capacity = null,
        $iconID = null
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->dogmaAttributes = $dogmaAttributes;
        $this->mass = $mass;
        $this->published = $published;
        $this->radius = $radius;
        $this->volume = $volume;
        $this->portionSize = $portionSize;
        $this->capacity = $capacity;
        $this->iconID = $iconID;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return PartialDogmaAttribute[]
     */
    public function getDogmaAttributes()
    {
        return $this->dogmaAttributes;
    }

    /**
     * @return int
     */
    public function getMass()
    {
        return $this->mass;
    }

    /**
     * @return boolean
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * @return int
     */
    public function getRadius()
    {
        return $this->radius;
    }

    /**
     * @return float
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * @return int
     */
    public function getPortionSize()
    {
        return $this->portionSize;
    }

    /**
     * @return Proxy[]
     */
    public function getProxies(): array
    {
        $proxies = [];
        foreach ($this->dogmaAttributes as $item) {
            if ($item instanceof IHasProxies) {
                array_push($proxies, ...$item->getProxies());
            }
        }

        return $proxies;
    }

    /**
     * @return float
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * @return int
     */
    public function getIconID()
    {
        return $this->iconID;
    }
}