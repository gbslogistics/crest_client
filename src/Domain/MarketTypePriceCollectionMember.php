<?php

namespace GbsLogistics\Crest\Domain;


use GbsLogistics\Crest\Proxy\Model\IHasProxies;
use GbsLogistics\Crest\Proxy\Proxy;

class MarketTypePriceCollectionMember implements IHasProxies
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /** @var float */
    private $adjustedPrice;

    /** @var float */
    private $averagePrice;

    /** @var Proxy */
    private $type;

    /**
     * MarketPrice constructor.
     * @param string $id
     * @param string $name
     * @param float $adjustedPrice
     * @param float $averagePrice
     * @param string $typeHref
     */
    public function __construct($id, $name, $adjustedPrice, $averagePrice, $typeHref)
    {
        $this->id = $id;
        $this->name = $name;
        $this->adjustedPrice = $adjustedPrice;
        $this->averagePrice = $averagePrice;
        $this->type = new Proxy($typeHref);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getAdjustedPrice()
    {
        return $this->adjustedPrice;
    }

    /**
     * @return float
     */
    public function getAveragePrice()
    {
        return $this->averagePrice;
    }

    /**
     * @return ItemType
     */
    public function getType()
    {
        return $this->type->resolve();
    }

    /**
     * @return Proxy[]
     */
    public function getProxies(): array
    {
        return [ $this->type ];
    }
}