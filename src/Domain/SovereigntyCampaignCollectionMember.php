<?php

namespace GbsLogistics\Crest\Domain;

use GbsLogistics\Crest\Domain\Partial\PartialSolarSystem;
use GbsLogistics\Crest\Domain\SovereigntyCampaign\Attackers;
use GbsLogistics\Crest\Domain\SovereigntyCampaign\Defender;
use GbsLogistics\Crest\Domain\SovereigntyCampaign\FreeportContestant;

class SovereigntyCampaignCollectionMember
{
    const TCU = 1;
    const IHUB = 2;
    const STATION = 3;
    const FREEPORT = 4;

    /** @var string */
    private $campaignID;

    /** @var string */
    private $eventType;

    /** @var string */
    private $sourceItemID;

    /** @var \DateTime */
    private $startTime;

    /** @var PartialSolarSystem */
    private $sourceSolarSystem;

    /** @var Attackers */
    private $attackers = null;

    /** @var Defender */
    private $defender = null;

    /** @var FreeportContestant[] */
    private $freeportContestants = null;

//    /** @var ParCon */
    private $constellation;

    /**
     * SovereigntyCampaignCollectionMember constructor.
     * @param string $campaignID
     * @param string $eventType
     * @param string $sourceItemID
     * @param \DateTime $startTime
     * @param PartialSolarSystem $sourceSolarSystem
     * @param Defender $defender
     * @param $antagonists
     */
    public function __construct($campaignID, $eventType, $sourceItemID, \DateTime $startTime, PartialSolarSystem $sourceSolarSystem, Defender $defender = null, $antagonists = null)
    {
        $this->campaignID = $campaignID;
        $this->eventType = $eventType;
        $this->sourceItemID = $sourceItemID;
        $this->startTime = $startTime;
        $this->sourceSolarSystem = $sourceSolarSystem;

        if (self::FREEPORT == $eventType) {
            if (is_array($antagonists)) {
                foreach ($antagonists as $antagonist) {
                    if (!($antagonist instanceof FreeportContestant)) {
                        throw new \InvalidArgumentException('Freeport events must have an array of antagonists composed of FreeportContestants.');
                    }
                }

                $this->freeportContestants = $antagonists;
            } elseif (null === $antagonists) {
                $this->freeportContestants = [];
            } else {
                throw new \InvalidArgumentException('Freeport event antagonists must be null or an array of FreeportContestants.');
            }

        } elseif (in_array($eventType, [ self::TCU, self::IHUB, self::STATION ])) {
            if (!($antagonists instanceof Attackers)) {
                throw new \InvalidArgumentException('Non-freeport events\' antagonist must be an object of type Attackers.');
            }

            $this->defender = $defender;
            $this->attackers = $antagonists;
        } else {
            throw new \InvalidArgumentException('Event type must be 1, 2, 3, or 4.');
        }
    }

    /**
     * @return string
     */
    public function getCampaignID()
    {
        return $this->campaignID;
    }

    /**
     * @return string
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * @return string
     */
    public function getSourceItemID()
    {
        return $this->sourceItemID;
    }

    /**
     * @return \DateTime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @return PartialSolarSystem
     */
    public function getSourceSolarSystem()
    {
        return $this->sourceSolarSystem;
    }

    /**
     * @return Attackers
     */
    public function getAttackers()
    {
        return $this->attackers;
    }

    /**
     * @return Defender
     */
    public function getDefender()
    {
        return $this->defender;
    }

    /**
     * @return SovereigntyCampaign\FreeportContestant[]
     */
    public function getFreeportContestants()
    {
        return $this->freeportContestants;
    }

    /**
     * @return mixed
     */
    public function getConstellation()
    {
        throw new \RuntimeException('getConstellation is not yet implemented.');
    }
}