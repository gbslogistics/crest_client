<?php

namespace GbsLogistics\Crest\Domain;


use GbsLogistics\Crest\Proxy\Model\IHasProxies;
use GbsLogistics\Crest\Proxy\Proxy;

class MarketGroup implements IHasProxies
{
    /** @var string */
    private $id;
    /** @var string */
    private $name;
    /** @var string */
    private $description;
    /** @var Proxy */
    private $types;
    /** @var Proxy|null */
    private $parentMarketGroup;

    /**
     * MarketGroup constructor.
     * @param string $id
     * @param string $name
     * @param string $description
     * @param $typesHref
     * @param $parentMarketGroupHref
     */
    public function __construct($id, $name, $description, $typesHref, $parentMarketGroupHref = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->types = new Proxy($typesHref);
        $this->parentMarketGroup = null === $parentMarketGroupHref
            ? null
            : new Proxy($parentMarketGroupHref);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return MarketTypeCollectionMember[]
     */
    public function getTypes()
    {
        return $this->types->resolve();
    }

    /**
     * @return MarketGroup|null
     */
    public function getParentMarketGroup()
    {
        if (null === $this->parentMarketGroup) {
            return null;
        }

        return $this->parentMarketGroup->resolve();
    }

    /**
     * @return Proxy[]
     */
    public function getProxies(): array
    {
        $proxies = [ $this->types ];
        if (null !== $this->parentMarketGroup) {
            $proxies[] = $this->parentMarketGroup;
        }

        return $proxies;
    }
}