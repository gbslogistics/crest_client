<?php

namespace GbsLogistics\Crest\Domain;


class Root
{
    /** @var string */
    private $dustUserCount;

    /** @var string */
    private $eveUserCount;

    /** @var string */
    private $serverVersion;

    /** @var string */
    private $dustStatus;

    /** @var string */
    private $eveStatus;

    /** @var string */
    private $serverStatus;

    /** @var string */
    private $serverName;

    /**
     * Root constructor.
     * @param string $dustUserCount
     * @param string $eveUserCount
     * @param string $serverVersion
     * @param string $dustStatus
     * @param string $eveStatus
     * @param string $serverStatus
     * @param string $serverName
     */
    public function __construct($dustUserCount, $eveUserCount, $serverVersion, $dustStatus, $eveStatus, $serverStatus, $serverName)
    {
        $this->dustUserCount = $dustUserCount;
        $this->eveUserCount = $eveUserCount;
        $this->serverVersion = $serverVersion;
        $this->dustStatus = $dustStatus;
        $this->eveStatus = $eveStatus;
        $this->serverStatus = $serverStatus;
        $this->serverName = $serverName;
    }

    /**
     * @return string
     */
    public function getDustUserCount()
    {
        return $this->dustUserCount;
    }

    /**
     * @return string
     */
    public function getEveUserCount()
    {
        return $this->eveUserCount;
    }

    /**
     * @return string
     */
    public function getServerVersion()
    {
        return $this->serverVersion;
    }

    /**
     * @return string
     */
    public function getDustStatus()
    {
        return $this->dustStatus;
    }

    /**
     * @return string
     */
    public function getEveStatus()
    {
        return $this->eveStatus;
    }

    /**
     * @return string
     */
    public function getServerStatus()
    {
        return $this->serverStatus;
    }

    /**
     * @return string
     */
    public function getServerName()
    {
        return $this->serverName;
    }
}