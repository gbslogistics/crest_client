<?php

namespace GbsLogistics\Crest\Domain;


use GbsLogistics\Crest\Domain\Partial\PartialAlliance;
use GbsLogistics\Crest\Domain\Partial\PartialItemType;
use GbsLogistics\Crest\Domain\Partial\PartialSolarSystem;

class SovereigntyStructureCollectionMember
{
    /** @var float */
    private $occupancyLevel;
    
    /** @var string */
    private $structureID;
    
    /** @var \DateTime */
    private $vulnerableStartTime;
    
    /** @var \DateTime */
    private $vulnerableEndTime;

    /** @var PartialSolarSystem */
    private $partialSolarSystem;

    /** @var PartialItemType */
    private $partialItemType;

    /** @var PartialAlliance */
    private $partialAlliance;

    /**
     * SovereigntyStructure constructor.
     * @param float $occupancyLevel
     * @param string $structureID
     * @param \DateTime $vulnerableStartTime
     * @param \DateTime $vulnerableEndTime
     * @param PartialSolarSystem $partialSolarSystem
     * @param PartialItemType $partialItemType
     * @param PartialAlliance $partialAlliance
     */
    public function __construct(
        $occupancyLevel,
        $structureID,
        \DateTime $vulnerableStartTime = null,
        \DateTime $vulnerableEndTime = null,
        PartialSolarSystem $partialSolarSystem = null,
        PartialItemType $partialItemType = null,
        PartialAlliance $partialAlliance = null
    ) {
        $this->occupancyLevel = $occupancyLevel;
        $this->structureID = $structureID;
        $this->vulnerableStartTime = $vulnerableStartTime;
        $this->vulnerableEndTime = $vulnerableEndTime;
        $this->partialSolarSystem = $partialSolarSystem;
        $this->partialItemType = $partialItemType;
        $this->partialAlliance = $partialAlliance;
    }

    /**
     * @return float
     */
    public function getOccupancyLevel()
    {
        return $this->occupancyLevel;
    }

    /**
     * @return string
     */
    public function getStructureID()
    {
        return $this->structureID;
    }

    /**
     * @return \DateTime
     */
    public function getVulnerableStartTime()
    {
        return $this->vulnerableStartTime;
    }

    /**
     * @return \DateTime
     */
    public function getVulnerableEndTime()
    {
        return $this->vulnerableEndTime;
    }

    /**
     * @return PartialSolarSystem
     */
    public function getPartialSolarSystem()
    {
        return $this->partialSolarSystem;
    }

    /**
     * @return PartialItemType
     */
    public function getPartialItemType()
    {
        return $this->partialItemType;
    }

    /**
     * @return PartialAlliance
     */
    public function getPartialAlliance()
    {
        return $this->partialAlliance;
    }
}