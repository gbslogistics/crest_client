<?php

namespace GbsLogistics\Crest\Domain;


use GbsLogistics\Crest\Domain\Partial\PartialSolarSystem;
use GbsLogistics\Crest\Domain\Partial\SystemCostIndex;
use GbsLogistics\Crest\Proxy\Model\IHasProxies;
use GbsLogistics\Crest\Proxy\Proxy;

class IndustrySystemCollectionMember implements IHasProxies
{
    /** @var PartialSolarSystem */
    private $partialSolarSystem;

    /** @var SystemCostIndex[] */
    private $systemCostIndices;

    /**
     * IndustrySystemCollectionMember constructor.
     * @param PartialSolarSystem $partialSolarSystem
     * @param Partial\SystemCostIndex[] $systemCostIndices
     */
    public function __construct(PartialSolarSystem $partialSolarSystem, array $systemCostIndices)
    {
        $this->partialSolarSystem = $partialSolarSystem;
        $this->systemCostIndices = $systemCostIndices;
    }

    /**
     * @return PartialSolarSystem
     */
    public function getPartialSolarSystem()
    {
        return $this->partialSolarSystem;
    }

    /**
     * @return Partial\SystemCostIndex[]
     */
    public function getSystemCostIndices()
    {
        return $this->systemCostIndices;
    }

    /**
     * @return Proxy[]
     */
    public function getProxies(): array
    {
        return $this->partialSolarSystem->getProxies();
    }
}