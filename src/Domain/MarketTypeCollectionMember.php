<?php

namespace GbsLogistics\Crest\Domain;


use GbsLogistics\Crest\Proxy\Model\IHasProxies;
use GbsLogistics\Crest\Proxy\Proxy;

class MarketTypeCollectionMember implements IHasProxies
{
    /** @var string */
    private $typeId;
    /** @var string */
    private $typeName;
    /** @var string */
    private $typeIconHref;
    /** @var Proxy */
    private $itemType;
    /** @var string */
    private $marketGroupId;
    /** @var Proxy */
    private $marketGroup;

    /**
     * MarketTypeCollectionMember constructor.
     * @param string $typeId
     * @param $typeName
     * @param $typeIconHref
     * @param $itemTypeHref
     * @param $marketGroupId
     * @param $marketGroupHref
     */
    public function __construct(
        $typeId,
        $typeName,
        $typeIconHref,
        $itemTypeHref,
        $marketGroupId,
        $marketGroupHref
    ) {
        $this->typeId = $typeId;
        $this->typeName = $typeName;
        $this->typeIconHref = $typeIconHref;
        $this->itemType = new Proxy($itemTypeHref);
        $this->marketGroupId = $marketGroupId;
        $this->marketGroup = new Proxy($marketGroupHref);
    }

    /**
     * @return string
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * @return string
     */
    public function getTypeName()
    {
        return $this->typeName;
    }

    /**
     * @return string
     */
    public function getTypeIconHref()
    {
        return $this->typeIconHref;
    }

    /**
     * @return ItemType
     */
    public function getItemType()
    {
        return $this->itemType->resolve();
    }

    /**
     * @return string
     */
    public function getMarketGroupId()
    {
        return $this->marketGroupId;
    }

    /**
     * @return MarketGroup
     */
    public function getMarketGroup()
    {
        return $this->marketGroup->resolve();
    }

    /**
     * @return Proxy[]
     */
    public function getProxies(): array
    {
        return [ $this->marketGroup, $this->itemType ];
    }
}