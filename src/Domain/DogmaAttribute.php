<?php

namespace GbsLogistics\Crest\Domain;


class DogmaAttribute
{
    // Required
    /** @var string */
    private $id;

    // Optional
    /** @var string */
    private $name;

    /** @var string */
    private $description;

    /** @var mixed */
    private $defaultValue;

    /** @var string */
    private $displayName;

    /** @var bool */
    private $highIsGood;

    /** @var int  */
    private $iconID;

    /** @var bool */
    private $published;

    /** @var bool */
    private $stackable;

    /** @var int */
    private $unitID;

    /**
     * DogmaAttribute constructor.
     * @param string $id
     * @param string $name
     * @param string $description
     * @param mixed $defaultValue
     * @param string $displayName
     * @param bool $highIsGood
     * @param int $iconID
     * @param bool $published
     * @param bool $stackable
     * @param int $unitID
     */
    public function __construct(
        $id,
        $name = null,
        $description = null,
        $defaultValue = null,
        $displayName = null,
        $highIsGood = null,
        $iconID = null,
        $published = null,
        $stackable = null,
        $unitID = null
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->defaultValue = $defaultValue;
        $this->displayName = $displayName;
        $this->highIsGood = $highIsGood;
        $this->iconID = $iconID;
        $this->published = $published;
        $this->stackable = $stackable;
        $this->unitID = $unitID;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * @return boolean
     */
    public function isHighIsGood()
    {
        return $this->highIsGood;
    }

    /**
     * @return int
     */
    public function getIconID()
    {
        return $this->iconID;
    }

    /**
     * @return boolean
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * @return boolean
     */
    public function isStackable()
    {
        return $this->stackable;
    }

    /**
     * @return int
     */
    public function getUnitID()
    {
        return $this->unitID;
    }
}