<?php

namespace GbsLogistics\Crest\Domain\Partial;


use GbsLogistics\Crest\Proxy\Proxy;

class PartialAlliance
{
    /** @var string */
    private $allianceID;

    /** @var string */
    private $allianceName;

    /** @var Proxy */
    private $alliance;

    /**
     * PartialAlliance constructor.
     * @param string $allianceID
     * @param string $allianceName
     * @param string $alliance
     */
    public function __construct($allianceID, $allianceName, string $allianceUrl)
    {
        $this->allianceID = $allianceID;
        $this->allianceName = $allianceName;
        $this->alliance = new Proxy($allianceUrl);
    }

    /**
     * @return string
     */
    public function getAllianceID()
    {
        return $this->allianceID;
    }

    /**
     * @return string
     */
    public function getAllianceName()
    {
        return $this->allianceName;
    }

    /**
     * @return Proxy
     */
    public function getAlliance()
    {
        throw new \RuntimeException('This method is not yet defined.');
//        return $this->alliance->resolve();
    }


}