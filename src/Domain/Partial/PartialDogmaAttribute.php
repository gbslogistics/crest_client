<?php

namespace GbsLogistics\Crest\Domain\Partial;


use GbsLogistics\Crest\Domain\DogmaAttribute;
use GbsLogistics\Crest\Proxy\Model\IHasProxies;
use GbsLogistics\Crest\Proxy\Proxy;

class PartialDogmaAttribute implements IHasProxies
{
    /** @var string */
    private $id;

    /** @var Proxy */
    private $attribute;

    /** @var string */
    private $name;

    /** @var float */
    private $value;

    /**
     * PartialDogmaAttribute constructor.
     * @param string $id
     * @param string $attributeHref
     * @param string $name
     * @param float $value
     */
    public function __construct($id, $attributeHref, $name, $value)
    {
        $this->id = $id;
        $this->attribute = new Proxy($attributeHref);
        $this->name = $name;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return DogmaAttribute
     */
    public function getAttribute()
    {
        return $this->attribute->resolve();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return Proxy[]
     */
    public function getProxies(): array
    {
        return [ $this->attribute ];
    }
}