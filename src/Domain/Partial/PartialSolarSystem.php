<?php

namespace GbsLogistics\Crest\Domain\Partial;


use GbsLogistics\Crest\Proxy\Model\IHasProxies;
use GbsLogistics\Crest\Proxy\Proxy;

class PartialSolarSystem implements IHasProxies
{
    /** @var string */
    private $id;
    /** @var string */
    private $name;
    /** @var Proxy */
    private $solarSystem;

    /**
     * PartialSolarSystem constructor.
     * @param string $id
     * @param string $name
     * @param string $solarSystemHref
     */
    public function __construct($id, $name, $solarSystemHref)
    {
        $this->id = $id;
        $this->name = $name;
        $this->solarSystem = new Proxy($solarSystemHref);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return SolarSystem
     */
    public function getSolarSystem()
    {
        return $this->solarSystem->resolve();
    }

    /**
     * @return Proxy[]
     */
    public function getProxies(): array
    {
        return [ $this->solarSystem ];
    }
}