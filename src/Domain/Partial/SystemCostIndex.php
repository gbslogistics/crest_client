<?php

namespace GbsLogistics\Crest\Domain\Partial;


class SystemCostIndex
{
    /** @var string */
    private $activityID;
    /** @var string */
    private $activityName;
    /** @var float */
    private $costIndex;

    /**
     * SystemCostIndex constructor.
     * @param string $activityID
     * @param string $activityName
     * @param float $costIndex
     */
    public function __construct($activityID, $activityName, $costIndex)
    {
        $this->activityID = $activityID;
        $this->activityName = $activityName;
        $this->costIndex = $costIndex;
    }

    /**
     * @return string
     */
    public function getActivityID()
    {
        return $this->activityID;
    }

    /**
     * @return string
     */
    public function getActivityName()
    {
        return $this->activityName;
    }

    /**
     * @return float
     */
    public function getCostIndex()
    {
        return $this->costIndex;
    }
}