<?php

namespace GbsLogistics\Crest\Domain\Partial;


use GbsLogistics\Crest\Domain\ItemType;
use GbsLogistics\Crest\Proxy\Model\IHasProxies;
use GbsLogistics\Crest\Proxy\Proxy;

class PartialItemType implements IHasProxies
{
    /** @var string */
    private $typeID;

    /** @var string */
    private $typeName;

    /** @var Proxy */
    private $itemType;

    /**
     * PartialItemType constructor.
     * @param string $typeID
     * @param string $typeName
     * @param string $itemTypeUrl
     */
    public function __construct($typeID, $typeName, $itemTypeUrl)
    {
        $this->typeID = $typeID;
        $this->typeName = $typeName;
        $this->itemType = new Proxy($itemTypeUrl);
    }

    /**
     * @return string
     */
    public function getTypeID()
    {
        return $this->typeID;
    }

    /**
     * @return string
     */
    public function getTypeName()
    {
        return $this->typeName;
    }

    /**
     * @return ItemType
     */
    public function getItemType()
    {
        return $this->itemType->resolve();
    }


    /**
     * @return Proxy[]
     */
    public function getProxies(): array
    {
        return [ $this->itemType ];
    }
}