<?php

namespace GbsLogistics\Crest\Domain;


class SolarSystem
{
    /** @var string */
    private $id;
    /** @var string */
    private $name;
    /** @var float */
    private $securityStatus;

    private $planets;

    /** @var Position */
    private $position;
    private $constellation;
    private $stats;
}