<?php

namespace GbsLogistics\Crest\Domain;


use GbsLogistics\Crest\Proxy\Proxy;

class Constellation
{
    /** @var string */
    private $name;
    /** @var string */
    private $positionX;
    /** @var string */
    private $positionY;
    /** @var string */
    private $positionZ;

    /** @var Region */
    private $region;

    private $systems;

    /**
     * Constellation constructor.
     * @param string $name
     * @param string $positionX
     * @param string $positionY
     * @param string $positionZ
     * @param string $region
     * @param string[] $systems
     */
    public function __construct($name, $positionX, $positionY, $positionZ, string $region, $systems)
    {
        $this->name = $name;
        $this->positionX = $positionX;
        $this->positionY = $positionY;
        $this->positionZ = $positionZ;
        $this->region = new Proxy($region);

        // TODO: Implement systems
        $this->systems = $systems;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPositionX()
    {
        return $this->positionX;
    }

    /**
     * @return string
     */
    public function getPositionY()
    {
        return $this->positionY;
    }

    /**
     * @return string
     */
    public function getPositionZ()
    {
        return $this->positionZ;
    }

    /**
     * @return Region
     */
    public function getRegion()
    {
        return $this->region->resolve();
    }

    /**
     * @return mixed
     */
    public function getSystems()
    {
        return $this->systems;
    }

}