<?php

namespace GbsLogistics\Crest\Domain;


class ItemTypeCollectionMember
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /** @var mixed */
    private $item;

    /**
     * ItemTypeMember constructor.
     * @param string $id
     * @param string $name
     * @param mixed $item
     */
    public function __construct($id, $name, $item)
    {
        $this->id = $id;
        $this->name = $name;
        $this->item = $item;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        // TODO: Proxy
        return $this->item;
    }
}