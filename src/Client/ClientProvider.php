<?php

namespace GbsLogistics\Crest\Client;


use GbsLogistics\Crest\Client\Model\IOptions;
use GbsLogistics\Crest\DomainMapper\MapperRegistry;
use GbsLogistics\Crest\Http\ContentTypeParser;
use GbsLogistics\Crest\Http\GuzzleFactory;
use GbsLogistics\Crest\Response\Factory\RootResponseFactory;
use GbsLogistics\Crest\Response\Parser;
use GbsLogistics\Crest\Response\RequestPool;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Client's responsibility is to provide a high-level API towards CREST.
 *
 * Class ClientProvider
 * @package GbsLogistics\Crest\Client
 */
class ClientProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        $pimple[ContentNegotiation::class] = function ($c) {
            return new ContentNegotiation(
                $c[ContentTypeParser::class],
                $c[MapperRegistry::class]
            );
        };

        $pimple[ClientFactory::class] = function (Container $c) {
            return new ClientFactory(function (IOptions $options) use ($c) {
                /** @var Container $c */
                return new Client(
                    $c[RootResponseFactory::class],
                    $c[ContentNegotiation::class],
                    $c[Parser::class],
                    $c[GuzzleFactory::class],
                    $options
                );
            });
        };
    }
}