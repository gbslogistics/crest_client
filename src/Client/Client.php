<?php

namespace GbsLogistics\Crest\Client;


use Concat\Http\Middleware\RateLimiter;
use GbsLogistics\Crest\Client\Model\IOptions;
use GbsLogistics\Crest\Http\GuzzleFactory;
use GbsLogistics\Crest\Http\Model\ContentType;
use GbsLogistics\Crest\Request\Model\IRequest;
use GbsLogistics\Crest\Response\Factory\RootResponseFactory;
use GbsLogistics\Crest\Response\Model\PooledRequest;
use GbsLogistics\Crest\Response\Model\WrappedResponse;
use GbsLogistics\Crest\Response\Parser;
use GbsLogistics\Crest\Response\RequestPool;
use GbsLogistics\Crest\Response\RootResponse;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Pool as GuzzlePool;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class Client implements IClient
{
    const ROOT_HREF = '/';

    /** @var GuzzleClient */
    private $guzzleClient;

    /** @var IOptions */
    private $options;

    /** @var RootResponseFactory */
    private $rootResponseFactory;

    /** @var ContentNegotiation */
    private $contentNegotiation;

    /** @var Parser */
    private $parser;

    /**
     * Client constructor.
     * @param RootResponseFactory $rootResponseFactory
     * @param ContentNegotiation $contentNegotiation
     * @param Parser $parser
     * @param GuzzleFactory $guzzleFactory
     * @param IOptions $options
     */
    public function __construct(
        RootResponseFactory $rootResponseFactory,
        ContentNegotiation $contentNegotiation,
        Parser $parser,
        GuzzleFactory $guzzleFactory,
        IOptions $options
    ) {
        $this->contentNegotiation = $contentNegotiation;
        $this->rootResponseFactory = $rootResponseFactory;
        $this->parser = $parser;
        $this->options = $options;
        $this->guzzleClient = $guzzleFactory->getInstance($options);
    }

    /**
     * @param string $href
     * @return array|object|null Returns an array if the href in question loads a multitude of objects, a single object
     *    if the href only has one object, or null if there was a problem.
     */
    public function getByHref(string $href)
    {
        if (null !== $href) {
            $pool = new RequestPool();
            $pool->addPooledRequest(new PooledRequest($href));

            $generator = $pool->getRequestsGenerator();

            /** @var PooledRequest $request */
            foreach ($generator as $request) {
                $response = $this->getCrestResource($request->getHref());
                if (null !== $response) {
                    $pool->processResponse($response);
                }
            }

            /** @var array $responseObjects */
            $responseObjects = $generator->getReturn();

            $proxyStrategy = $this->options->getProxyStrategy();
            $proxyStrategy->registerResolutionFunctions(function (string ...$hrefs) {
                return $this->getCrestResources(...$hrefs);
            }, $responseObjects);

            return 1 === count($responseObjects) ? $responseObjects[0] : $responseObjects;
        }

        return null;
    }

    public function getByResource(IRequest $request)
    {
        $root = $this->getRoot();
        return null === $root ? null : $this->getByHref($root->getResource($request->getPath()));
    }

    /**
     * Grabs the root.
     *
     * @return RootResponse|null
     */
    private function getRoot()
    {
        $rawResponse = $this->getCrestResource(self::ROOT_HREF);

        return null === $rawResponse ? null : $this->rootResponseFactory->build(
            $rawResponse->getJsonData(),
            $rawResponse->getResponseObject()
        );
    }

    /**
     * @param string $href
     * @return null|WrappedResponse
     */
    private function getCrestResource(string $href)
    {
        $response = $this->getCrestResources($href);
        return count($response) ? array_shift($response) : null;
    }

    /**
     * Performs content negotiation and makes a call to CREST.
     *
     * @param string[] $hrefs
     * @return WrappedResponse[] Keys to this array will be the requesting href.
     */
    private function getCrestResources(string ...$hrefs)
    {
        $contentTypes = $this->contentNegotiation->negotiate($this->guzzleClient, $this->options, ...$hrefs);
        $requests = array_map(function (ContentType $contentType) {
            $headers = $contentType ? [ 'Accept' => (string)$contentType ] : [];
            return new Request('GET', $contentType->getHref(), $headers);
        }, $contentTypes);

        $responses = [];
        $pool = new GuzzlePool($this->guzzleClient, $requests, [
            'concurrency' => $this->options->getConcurrencyLimit(),
            'fulfilled' => function (ResponseInterface $response, $index) use (&$responses, $requests) {
                /** @var RequestInterface $request */
                $request = $requests[$index];
                $responses[(string)$request->getUri()] = $this->parser->parse($response);
            },
            'rejected' => function ($reason, $index) {
                // TODO: Handle rejected CREST GET
            }
        ]);

        $pool->promise()->wait();

        return $responses;
    }
}