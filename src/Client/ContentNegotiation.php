<?php

namespace GbsLogistics\Crest\Client;


use GbsLogistics\Crest\Client\Model\IOptions;
use GbsLogistics\Crest\DomainMapper\IMapper;
use GbsLogistics\Crest\DomainMapper\MapperRegistry;
use GbsLogistics\Crest\Http\ContentTypeParser;
use GbsLogistics\Crest\Http\Model\ContentType;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\RequestInterface;

class ContentNegotiation
{
    /** @var ContentTypeParser */
    private $contentTypeParser;

    /** @var MapperRegistry */
    private $mapperRegistry;

    /**
     * ContentNegotiation constructor.
     * @param ContentTypeParser $contentTypeParser
     * @param MapperRegistry $mapperRegistry
     */
    public function __construct(ContentTypeParser $contentTypeParser, MapperRegistry $mapperRegistry)
    {
        $this->contentTypeParser = $contentTypeParser;
        $this->mapperRegistry = $mapperRegistry;
    }

    /**
     * Interrogates the CREST server to determine the resource type being pulled, then compares what version it has to
     * which version it requires.
     *
     * @param ClientInterface $client
     * @param IOptions $options
     * @param string[] ...$hrefs
     * @return ContentType[] Note: may return fewer ContentType objects than what was provided in the arguments.
     */
    public function negotiate(ClientInterface $client, IOptions $options, string ...$hrefs)
    {
        try {
            $requests = array_map(function ($href) {
                return new Request('HEAD', $href);
            }, $hrefs);

            $contentTypes = [];
            $pool = new Pool($client, $requests, [
                'concurrency' => $options->getConcurrencyLimit(),

                'fulfilled' => function (Response $response, $index) use (&$contentTypes, $requests) {
                    /** @var RequestInterface $request */
                    $request = $requests[$index];
                    $href = $request->getUri();
                    $contentType = $this->contentTypeParser->parse($response);

                    if (null !== $contentType) {
                        $resourceName = $contentType->getResourceName();
                        /** @var IMapper $mapper */
                        $mapper = $this->mapperRegistry->getMapperByResourceName($resourceName);

                        if (null !== $mapper) {
                            if ($contentType->getVersion() == $mapper->getVersion()) {
                                $contentTypes[] = new ContentType(
                                    $contentType->getResourceName(),
                                    $contentType->getVersion(),
                                    $href
                                );
                            }

                            // TODO: Send deprecation notification
                            $contentTypes[] = new ContentType($resourceName, $mapper->getVersion(), $href);
                        } else {
                            // We don't have a mapper for this resource. We'll return the most recent content type, and
                            // let the parser just generate a generic response.
                            $contentTypes[] = new ContentType(
                                $contentType->getResourceName(),
                                $contentType->getVersion(),
                                $href
                            );
                        }
                    } else {
                        // TODO: Handle content-type parsing error
                    }
                },

                'rejected' => function ($reason, $index) {
                    // TODO: Handle rejected HEAD lookups
                }
            ]);

            $pool->promise()->wait();

            return $contentTypes;
        } catch (\Exception $e) {
            // TODO: Handle HEAD request error
            return [];
        }
    }
}