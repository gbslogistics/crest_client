<?php

namespace GbsLogistics\Crest\Client;



use GbsLogistics\Crest\Client\Model\IOptions;

class ClientFactory implements IClientFactory
{
    /** @var callable */
    private $clientInvoker;

    /**
     * ClientFactory constructor.
     * @param callable $clientInvoker
     */
    public function __construct(callable $clientInvoker)
    {
        $this->clientInvoker = $clientInvoker;
    }

    /**
     * Returns an instance of the client.
     *
     * @param IOptions $options
     * @return IClient
     */
    public function getClient(IOptions $options): IClient
    {
        /** @var Client $client */
        $client = call_user_func($this->clientInvoker, $options);

        return $client;
    }
}