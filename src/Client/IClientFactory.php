<?php

namespace GbsLogistics\Crest\Client;



use GbsLogistics\Crest\Client\Model\IOptions;

interface IClientFactory
{
    /**
     * Returns an instance of the client.
     *
     * @param IOptions $options
     * @return IClient
     */
    public function getClient(IOptions $options): IClient;
}