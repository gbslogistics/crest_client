<?php

namespace GbsLogistics\Crest\Client\Model;


use Concat\Http\Middleware\RateLimitProvider;
use GbsLogistics\Crest\Proxy\Strategy\AbstractProxyStrategy;
use GuzzleHttp\HandlerStack;

interface IOptions
{
    public function getRootURI(): string;

    public function getConnectTimeout(): int;

    public function getUserAgent(): string;

    public function getProxyStrategy(): AbstractProxyStrategy;

    public function getConcurrencyLimit(): int;

    public function getRateLimitProvider(): RateLimitProvider;

    /**
     * @return HandlerStack|null
     */
    public function getGuzzleHandlerStack();
}