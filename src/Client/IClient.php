<?php

namespace GbsLogistics\Crest\Client;


use GbsLogistics\Crest\Response\Model\IResponse;
use GbsLogistics\Crest\Request\Model\IRequest;
use GuzzleHttp\ClientInterface;

interface IClient
{
    public function getByHref(string $href);

    public function getByResource(IRequest $request);
}