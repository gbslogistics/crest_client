<?php

namespace GbsLogistics\Crest\Http;


use Concat\Http\Middleware\RateLimiter;
use GbsLogistics\Crest\Client\Model\IOptions;
use GbsLogistics\Crest\OptionsBuilder;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;

class GuzzleFactory
{
    public function getInstance(IOptions $options = null): Client
    {
        if (null === $options) {
            $options = (new OptionsBuilder('https://public-crest.eveonline.com'))->build();
        }

        $stack = $options->getGuzzleHandlerStack();
        if (null === $stack) {
            $stack = new HandlerStack();
            $stack->setHandler(new CurlHandler());
        }

        $stack->push(new RateLimiter($options->getRateLimitProvider()));

        return new Client([
            'base_uri' => $options->getRootURI(),
            'connect_timeout' => $options->getConnectTimeout(),
            'user_agent' => $options->getUserAgent(),
            'handler' => $stack,
//            'debug' => true
        ]);
    }
}