<?php

namespace GbsLogistics\Crest\Http;


use GbsLogistics\Crest\Client\ContentNegotiation;
use GbsLogistics\Crest\DomainMapper\MapperRegistry;
use GbsLogistics\Crest\Http\ContentTypeParser;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Http's responsibility is to dispatch low-level HTTP requests and manage the guzzle instance.
 *
 * Class HttpProvider
 * @package GbsLogistics\Crest\Http
 */
class HttpProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        $pimple[ContentTypeParser::class] = function ($c) {
            return new ContentTypeParser();
        };

        $pimple[GuzzleFactory::class] = function ($c) {
            return new GuzzleFactory();
        };
    }
}