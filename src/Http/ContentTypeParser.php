<?php

namespace GbsLogistics\Crest\Http;


use GbsLogistics\Crest\Http\Model\ContentType;
use Psr\Http\Message\ResponseInterface;

class ContentTypeParser
{
    const PATTERN = '#application/vnd.ccp.eve.(\w+)-v(\d+)\+json#';
    const CONTENT_TYPE_HEADER = 'Content-Type';

    /**
     *
     * @param ResponseInterface $rawResponse
     * @return ContentType|null Returns the parsed ContentType, or null if what we got back isn't a valid content type.
     *    Nulls can often arise from an error from CREST; it's recommended to further parse the response for an error.
     */
    public function parse(ResponseInterface $rawResponse)
    {
        $headerArray = $rawResponse->getHeader(self::CONTENT_TYPE_HEADER);
        $rawContentType = '';
        if (is_array($headerArray) && count($headerArray) > 0) {
            $rawContentType = $headerArray[0];
        }

        if (preg_match(self::PATTERN, $rawContentType, $matches)) {
            return new ContentType($matches[1], $matches[2]);
        }

        return null;
    }
}