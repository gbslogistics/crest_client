<?php

namespace GbsLogistics\Crest\Http\Model;


class ContentType
{
    /** @var string */
    private $resourceName;

    /** @var string */
    private $version;

    /** @var bool */
    private $isCollection;

    /** @var string */
    private $href;

    /**
     * ContentType constructor.
     * @param string $resourceName
     * @param string $version
     * @param null $href
     */
    public function __construct($resourceName, $version, $href = null)
    {
        $this->resourceName = $resourceName;
        $this->version = $version;
        $this->isCollection = preg_match('#Collection$#', $this->getResourceName());
        $this->href = $href;
    }

    /**
     * @return string
     */
    public function getResourceName()
    {
        return $this->resourceName;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    public function isCollection()
    {
        return $this->isCollection;
    }

    public function __toString()
    {
        return sprintf('application/vnd.ccp.eve.%s-v%s+json', $this->getResourceName(), $this->getVersion());
    }

    /**
     * @return string
     */
    public function getHref()
    {
        return $this->href;
    }
}