<?php

namespace GbsLogistics\Crest\Json;


interface IJsonData
{
    /**
     * @return \Generator Generator contains scalar values and instances of IJsonData.
     */
    public function getRootGenerator(): \Generator;

    public function getRootArray();

    /**
     * @param string $key
     * @return string|number|IJsonData|null A scalar value, or an instance of IJsonData. Returns null if the value is
     *     not found.
     */
    public function getJsonDataByFirstLevelKey(string $key);
}