<?php

namespace GbsLogistics\Crest\Json;


use Psr\Http\Message\StreamInterface;

class NativeJsonParser implements IJsonParser
{
    public function parse(StreamInterface $stream): IJsonData
    {
        $jsonData = json_decode($stream->getContents(), true);

        if (JSON_ERROR_NONE === json_last_error()) {
            return new InMemoryJsonData($jsonData);
        } else {
            throw new \RuntimeException('Invalid JSON data.');
        }
    }
}