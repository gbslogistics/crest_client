<?php

namespace GbsLogistics\Crest\Json;


use Psr\Http\Message\StreamInterface;

/**
 * Abstracts JSON parsing away from the client.
 *
 * This abstraction was done so that it would be easy to replace the native json_decode() call with a JSON pull parser,
 * should performance needs require it.
 *
 * Interface IJsonParser
 * @package GbsLogistics\Crest\Json
 */
interface IJsonParser
{
    public function parse(StreamInterface $stream): IJsonData;
}