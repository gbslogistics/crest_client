<?php

namespace GbsLogistics\Crest\Json;


use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Json's responsibility is to abstract the processing of and iteration over JSON data, so that a pull parser could
 * later be implemented if it was deemed worthwhile.
 *
 * Class JsonProvider
 * @package GbsLogistics\Crest\Json
 */
class JsonProvider implements ServiceProviderInterface
{

    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        $pimple[NativeJsonParser::class] = function ($c) {
            return new NativeJsonParser();
        };

        $pimple['json_parser'] = $pimple->raw(NativeJsonParser::class);
    }
}