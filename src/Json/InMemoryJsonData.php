<?php

namespace GbsLogistics\Crest\Json;


class InMemoryJsonData implements IJsonData
{
    /** @var array */
    private $jsonData;

    /**
     * InMemoryJsonData constructor.
     * @param array $jsonData
     */
    public function __construct(array $jsonData)
    {
        $this->jsonData = $jsonData;
    }

    /**
     * @return \Generator Generator contains scalar values and instances of IJsonData.
     */
    public function getRootGenerator(): \Generator
    {
        foreach ($this->jsonData as $key => $value) {
            if (is_array($value)) {
                yield $key => new self($value);
            } else {
                yield $key => $value;
            }
        }
    }

    public function getRootArray()
    {
        return $this->jsonData;
    }

    /**
     * @param string $key
     * @return string|number|IJsonData|null A scalar value, or an instance of IJsonData. Returns null if the value is
     *     not found.
     */
    public function getJsonDataByFirstLevelKey(string $key)
    {
        if (isset($this->jsonData[$key])) {
            $data = $this->jsonData[$key];

            if (is_array($data)) {
                return new self($data);
            }

            return $data;
        }

        return null;
    }
}